<?xml version="1.0">
<nidocument>
<nicomment>
<nifamily displayname="Analytics and Machine Learning" familyname="Analytics and Machine Learning">
</nifamily>
</nicomment>
<nierror code="-388410">
The array size of <b>weights</b> or <b>biases</b> must equal the number of hidden layers. The array size of <b>weight</b> or <b>bias</b> must equal the number of neurons in each layer.
</nierror>
<nierror code="-388409">
The array size of <b>hidden layer type</b> must equal the array size of <b>number of hidden neurons</b>.
</nierror>
<nierror code="-388408">
The model fitting does not converge before reaching the maximum number of iterations.
</nierror>
<nierror code="-388407">
The SVM model training fails. Normalize the training data or change the SVM parameters.
</nierror>
<nierror code="-388406">
The GMM model training fails. Increase <b>regulator</b>.
</nierror>
<nierror code="-388405">
The number of data samples must be greater than 1.
</nierror>
<nierror code="-388404">
There must be at least one cluster in <b>predicted labels</b> that has more than one data samples for the Davies Bouldin Index metric and the Dunn Index metric.
</nierror>
<nierror code="-388403">
<b>predicted labels</b> must have more than one clusters for the Davies Bouldin Index metric and the Dunn Index metric.
</nierror>
<nierror code="-388402">
The number of predicted labels in <b>predicted labels</b> must equal the number of data samples.
</nierror>
<nierror code="-388401">
The number of data samples must be no less than the number of clusters.
</nierror>
<nierror code="-388400">
The covariance matrix in <b>covariance matrix</b> must be positive definite.
</nierror>
<nierror code="-388399">
The covariance matrix in <b>initial covariance matrix</b> must be positive definite.
</nierror>
<nierror code="-388398">
The number of columns of <b>initial centroids</b> must equal the number of features in the training data.
</nierror>
<nierror code="-388397">
The covariance matrix in <b>initial covariance matrix</b> must be symmetric.
</nierror>
<nierror code="-388396">
The number of rows or columns of <b>initial covariance matrix</b> must equal the number of features in the training data.
</nierror>
<nierror code="-388395">
<b>initial weights</b> must be between 0 and 1.
</nierror>
<nierror code="-388394">
The number of columns of <b>initial mean values</b> must equal the number of features in data.
</nierror>
<nierror code="-388393">
<b>threshold for T2</b> must be greater than or equal to 0.
</nierror>
<nierror code="-388392">
The wrong Load Data (2D Array) VI instance is used to load data.
</nierror>
<nierror code="-388391">
The array length of <b>number of support vectors for each class</b> must equal <b>number of classes</b>.
</nierror>
<nierror code="-388390">
You must perform feature manipulation before training clustering, classification, and anomaly detection models.
</nierror>
<nierror code="-388389">
<b>nu</b> is invalid. Reduce the value of nu.
</nierror>
<nierror code="-388388">
The array size of <b>variance</b> must equal the number of eigenvectors.
</nierror>
<nierror code="-388387">
<b>MQE threshold</b> must be greater than or equal to 0.
</nierror>
<nierror code="-388386">
The number of features in the input data must equal the number of columns of <b>support vectors</b>.
</nierror>
<nierror code="-388385">
The number of nodes, which equals <b>row</b> * <b>column</b>, must equal the number of columns of <b>map vectors</b>.
</nierror>
<nierror code="-388384">
<b>confidence level</b> must be greater than 0 and less than 1.
</nierror>
<nierror code="-388383">
<b>initial learning rate</b> must be greater than 0.
</nierror>
<nierror code="-388382">
<b>initial radius</b> must be greater than 0.
</nierror>
<nierror code="-388381">
The value of <b>row</b> and <b>col</b> must be greater than or equal to 0.
</nierror>
<nierror code="-388380">
The number of features in the input data must equal the number of features in <b>map vectors</b>.
</nierror>
<nierror code="-388379">
<b>covariance matrix</b> must be symmetric.
</nierror>
<nierror code="-388378">
You must not choose the AIC metric or the BIC metric to evaluate the DBSCAN model and the K-Means model.
</nierror>
<nierror code="-388377">
<b>threshold for Q</b> must be greater than or equal to 0.
</nierror>
<nierror code="-388376">
The number of columns of the input data must equal the number of columns of <b>centroids</b>.
</nierror>
<nierror code="-388375">
The number of columns of the input data must equal the number of columns of <b>core samples</b>.
</nierror>
<nierror code="-388374">
The number of columns of the input data must equal the number of columns of <b>mean values</b>.
</nierror>
<nierror code="-388372">
The size of <b>predicted labels</b> must equal the size of <b>input labels</b>.
</nierror>
<nierror code="-388371">
<b>centroids</b> must not be empty and the number of rows of <b>centroids</b> must equal <b>number of clusters</b>.
</nierror>
<nierror code="-388370">
The size of <b>covariance values</b> must equal <b>number of clusters</b> and the number of rows/columns of <b>covariance matrix</b> must equal the size of <b>mean values</b>.
</nierror>
<nierror code="-388369">
The size of <b>weights</b> must equal <b>number of clusters</b> and the sum of <b>weights</b> must equal 1.
</nierror>
<nierror code="-388368">
The number of rows of <b>mean values</b> must equal <b>number of clusters</b>.
</nierror>
<nierror code="-388367">
The size of <b>core samples labels</b> must be greater than 0.
</nierror>
<nierror code="-388366">
<b>max</b> must be greater than <b>min</b>.
</nierror>
<nierror code="-388365">
The size of <b>core sample labels</b> must equal the number of rows of <b>core samples</b>.
</nierror>
<nierror code="-388364">
<b>initial method</b> must not be empty.
</nierror>
<nierror code="-388362">
<b>attempts</b> must be greater than 0.
</nierror>
<nierror code="-388361">
<b>p</b> must be greater than 0.
</nierror>
<nierror code="-388360">
<b>min samples</b> must be greater than 0.
</nierror>
<nierror code="-388359">
<b>max distance</b> must be greater than 0.
</nierror>
<nierror code="-388358">
<b>number of clusters</b> must be greater than 0.
</nierror>
<nierror code="-388357">
<b>regulator</b> must be greater than 0.
</nierror>
<nierror code="-388356">
Out of memory.
</nierror>
<nierror code="-388355">
The sum of <b>number of support vectors for each class</b> must equal <b>number of support vectors</b>.
</nierror>
<nierror code="-388354">
<b>output layer type</b> is empty.
</nierror>
<nierror code="-388353">
The array size of <b>probB</b> must equal number of classes * (number of classes - 1)/2.
</nierror>
<nierror code="-388352">
The array size of <b>probA</b> must equal number of classes * (number of classes - 1)/2.
</nierror>
<nierror code="-388351">
The array size of <b>decision function constants</b> must equal number of classes * (number of classes - 1)/2.
</nierror>
<nierror code="-388350">
The number of rows of <b>coefficients of support vectors</b> must be one less than <b>number of classes</b>, and the number of columns of <b>coefficients of support vectors</b> must equal <b>number of support vectors</b>.
</nierror>
<nierror code="-388349">
The number of rows of <b>support vectors</b> must equal <b>number of support vectors</b>.
</nierror>
<nierror code="-388348">
<b>number of support vectors</b> must be greater than 0.
</nierror>
<nierror code="-388347">
The number of rows of <b>H to O coefficients</b> must equal <b>number of output neurons</b>, and the number of columns of <b>H to O coefficients</b> must be one greater than <b>number of hidden neurons</b>.
</nierror>
<nierror code="-388346">
The number of rows of <b>I to H coefficients</b> must equal <b>number of hidden neurons</b>, and the number of columns of <b>I to H coefficients</b> must be one greater than <b>number of input neurons</b>.
</nierror>
<nierror code="-388345">
<b>number of output neurons</b> must equal <b>number of classes</b>.
</nierror>
<nierror code="-388344">
The number of features in the input data must equal <b>number of input neurons</b>.
</nierror>
<nierror code="-388343">
<b>number of input neurons</b> must be greater than 0.
</nierror>
<nierror code="-388342">
For multi-class classification, the number of rows of <b>coefficients of support vectors</b> must equal <b>number of classes</b>. For two-class classification, the number of rows of <b>coefficients of support vectors</b> must equal 1.
</nierror>
<nierror code="-388341">
The array size of <b>labels of each class</b> must equal <b>number of classes</b>.
</nierror>
<nierror code="-388340">
<b>number of classes</b> must be greater than 1.
</nierror>
<nierror code="-388339">
The number of features in the input data must be one less than the number of columns of <b>coefficients of support vectors</b>.
</nierror>
<nierror code="-388338">
The model is untrained before deployment.
</nierror>
<nierror code="-388337">
<b>cost function type</b> must not be empty.
</nierror>
<nierror code="-388336">
You must initialize the model before training.
</nierror>
<nierror code="-388335">
<b>coef0</b> must not be empty.
</nierror>
<nierror code="-388334">
<b>kernel type</b> must not be empty.
</nierror>
<nierror code="-388333">
<b>SVM type</b> must not be empty.
</nierror>
<nierror code="-388332">
<b>hidden layer type</b> must not be empty.
</nierror>
<nierror code="-388331">
There are repeated class labels in <b>weighted c</b>.
</nierror>
<nierror code="-388330">
<b>class weight</b> of <b>weighted c</b> must be greater than 0.
</nierror>
<nierror code="-388329">
<b>label</b> of <b>weighted c</b> cannot be found in the labels of the training data.
</nierror>
<nierror code="-388328">
<b>nu</b> must be greater than 0 and less than or equal to 1.
</nierror>
<nierror code="-388327">
<b>c</b> must be greater than 0.
</nierror>
<nierror code="-388326">
<b>number of hidden neurons</b> must be greater than 0.
</nierror>
<nierror code="-388325">
<b>number of searches</b> must be greater than 0 when <b>search method</b> is random search.
</nierror>
<nierror code="-388324">
<b>positive label</b> cannot be found in the labels of the input data.
</nierror>
<nierror code="-388323">
<b>number of folds</b> must be greater than 1 and equal to or less than the number of samples in the training data.
</nierror>
<nierror code="-388322">
<b>tolerance</b> must be greater than 0.
</nierror>
<nierror code="-388321">
<b>max iteration</b> must be greater than 0.
</nierror>
<nierror code="-388320">
The input data must not be empty.
</nierror>
<nierror code="-388319">
<b>ratio of variance</b> must be greater than 0 and no greater than 1.
</nierror>
<nierror code="-388318">
The number of classes in the training data must be no greater than 2.
</nierror>
<nierror code="-388317">
<b>number of components</b> must be greater than 0 and no greater than the number of features in training data.
</nierror>
<nierror code="-388316">
The number of samples in the training data must equal <b>number of labels</b>.
</nierror>
<nierror code="-388315">
The number of features in the input data must equal the number of rows of <b>eigenvectors</b>.
</nierror>
<nierror code="-388314">
The number of rows of <b>eigenvectors</b> must equal the array size of <b>mean values</b>, and be no less than the number of columns of <b>eigenvectors</b>.
</nierror>
<nierror code="-388313">
<b>mean values</b> and <b>eigenvectors</b> must not be empty.
</nierror>
<nierror code="-388312">
<b>selected feature index</b> must be less than the number of features in the input data.
</nierror>
<nierror code="-388311">
The selected feature indexes in <b>selected feature index</b> must be different from each other.
</nierror>
<nierror code="-388310">
<b>selected feature index</b> must not be empty.
</nierror>
<nierror code="-388309">
The number of features in the input data must equal the array size of <b>min values</b> or <b>max values</b>.
</nierror>
<nierror code="-388308">
The number of features in the input data must equal the array size of <b>mean values</b>.
</nierror>
<nierror code="-388307">
<b>min values</b> and <b>max values</b> must not be empty. The size of <b>min values</b> and <b>max values</b> must be the same.
</nierror>
<nierror code="-388306">
<b>mean values</b> and <b>standard deviation values</b> must not be empty. The size of <b>mean values</b> and <b>standard deviation values</b> must be the same.
</nierror>
<nierror code="-388305">
The number of features in the input data must equal that in the source data.
</nierror>
<nierror code="-388304">
<b>gamma</b> must be greater than 0.
</nierror>
<nierror code="-388303">
<b>degree</b> must be greater than 0.
</nierror>
<nierror code="-388302">
The array sizes of <b>source data</b>, <b>eigenvectors</b>, and <b>variance</b> in the input model are not compatible with each other.
</nierror>
<nierror code="-388301">
<b>source data</b>, <b>eigenvectors</b>, and <b>variance</b> in the input model must not be empty.
</nierror>
<nierror code="-388300">
The data must be loaded.
</nierror>
<nierror code="388301">
The PCA T2Q model is trained based on the data that is already transformed with PCA feature manipulation.
</nierror>
</nidocument>
