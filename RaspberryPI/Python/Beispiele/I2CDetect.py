# I2CDetect.py

import smbus

def getI2CDevices():
    bus = smbus.SMBus(1)
    addresses = [] 
    for address in range(128):
        try:
            bus.read_byte(address)
            addresses.append(address)
        except:
            pass
    return addresses

print "Found I2C devices at:"
devices = getI2CDevices()
for address in devices:
    print("0x" + format(address, "02X"))

