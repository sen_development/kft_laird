﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Property Name="Instrument Driver" Type="Str">True</Property>
	<Property Name="NI.Project.Description" Type="Str">This project is used by developers to edit API and example files for LabVIEW Plug and Play instrument drivers.</Property>
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="CCSymbols" Type="Str">OS,Win;CPU,x86;</Property>
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Examples" Type="Folder">
			<Item Name="Playground.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DL3000 Series/Examples/Playground.vi"/>
		</Item>
		<Item Name="RIGOL DL3000 Series.lvlib" Type="Library" URL="/&lt;instrlib&gt;/RIGOL DL3000 Series/RIGOL DL3000 Series.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
			</Item>
			<Item Name="instr.lib" Type="Folder">
				<Item Name="Configure Trigger Delay.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DL3000 Series/Public/Configure/Low Level/Configure Trigger Delay.vi"/>
				<Item Name="GetInputState.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DL3000 Series/Public/Configure/GetInputState.vi"/>
				<Item Name="GetTriggerSource.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DL3000 Series/Public/Configure/GetTriggerSource.vi"/>
				<Item Name="GetOperationMode.vi" Type="VI" URL="/&lt;instrlib&gt;/RIGOL DL3000 Series/Public/Configure/GetOperationMode.vi"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
