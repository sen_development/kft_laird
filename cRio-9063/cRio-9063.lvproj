﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Property Name="NI.LV.ExampleFinder" Type="Str">&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;ExampleProgram&gt;
&lt;Title&gt;
	&lt;Text Locale="US"&gt;9411 Digital Line Input - cRIO.lvproj&lt;/Text&gt;
&lt;/Title&gt;
&lt;Keywords&gt;
	&lt;Item&gt;CompactRIO&lt;/Item&gt;
	&lt;Item&gt;FPGA&lt;/Item&gt;
	&lt;Item&gt;started&lt;/Item&gt;
	&lt;Item&gt;getting&lt;/Item&gt;
	&lt;Item&gt;modules&lt;/Item&gt;
	&lt;Item&gt;line&lt;/Item&gt;
	&lt;Item&gt;digital&lt;/Item&gt;
	&lt;Item&gt;cRIO&lt;/Item&gt;
	&lt;Item&gt;Input&lt;/Item&gt;
	&lt;Item&gt;9425&lt;/Item&gt;
	&lt;Item&gt;DI&lt;/Item&gt;
	&lt;Item&gt;di&lt;/Item&gt;
&lt;/Keywords&gt;
&lt;Navigation&gt;
	&lt;Item&gt;7428&lt;/Item&gt;
	&lt;Item&gt;7691&lt;/Item&gt;
&lt;/Navigation&gt;
&lt;FileType&gt;LV Project&lt;/FileType&gt;
&lt;Metadata&gt;
&lt;Item Name="RTSupport"&gt;LV Project RT&lt;/Item&gt;
&lt;/Metadata&gt;
&lt;ProgrammingLanguages&gt;
&lt;Item&gt;LabVIEW&lt;/Item&gt;
&lt;/ProgrammingLanguages&gt;
&lt;RequiredSoftware&gt;
&lt;NiSoftware MinVersion="14.0"&gt;LabVIEW&lt;/NiSoftware&gt; 
&lt;/RequiredSoftware&gt;
&lt;RequiredFPGAHardware&gt;
&lt;Device&gt;
&lt;Model&gt;712F&lt;/Model&gt;
&lt;/Device&gt;
&lt;/RequiredFPGAHardware&gt;
&lt;/ExampleProgram&gt;</Property>
	<Property Name="NI.Project.Description" Type="Str">This project demonstrates how to acquire digital signals on 8 channels using the NI 9425.
 
This example needs to be compiled for a specific FPGA target before use.

For information on moving this example to another FPGA target, refer to ni.com/info and enter info code fpgaex.</Property>
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Host" Type="Folder" URL="../Host">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="ReadfromTcp.vi" Type="VI" URL="../Sub VIs/ReadfromTcp.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="subTimeDelay.vi" Type="VI" URL="/&lt;vilib&gt;/express/express execution control/TimeDelayBlock.llb/subTimeDelay.vi"/>
			</Item>
			<Item Name="ReadfromTcp.vi" Type="VI" URL="../Host/ReadfromTcp.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
	<Item Name="NI-cRIO-9063-01dc53a6" Type="RT CompactRIO">
		<Property Name="alias.name" Type="Str">NI-cRIO-9063-01dc53a6</Property>
		<Property Name="alias.value" Type="Str">192.168.10.41</Property>
		<Property Name="CCSymbols" Type="Str">TARGET_TYPE,RT;OS,Linux;CPU,ARM;DeviceCode,7740;</Property>
		<Property Name="crio.ControllerPID" Type="Str">7740</Property>
		<Property Name="host.ResponsivenessCheckEnabled" Type="Bool">true</Property>
		<Property Name="host.ResponsivenessCheckPingDelay" Type="UInt">5000</Property>
		<Property Name="host.ResponsivenessCheckPingTimeout" Type="UInt">1000</Property>
		<Property Name="host.TargetCPUID" Type="UInt">8</Property>
		<Property Name="host.TargetOSID" Type="UInt">8</Property>
		<Property Name="target.cleanupVisa" Type="Bool">false</Property>
		<Property Name="target.FPProtocolGlobals_ControlTimeLimit" Type="Int">300</Property>
		<Property Name="target.getDefault-&gt;WebServer.Port" Type="Int">80</Property>
		<Property Name="target.getDefault-&gt;WebServer.Timeout" Type="Int">60</Property>
		<Property Name="target.IOScan.Faults" Type="Str"></Property>
		<Property Name="target.IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="target.IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="target.IOScan.Period" Type="UInt">10000</Property>
		<Property Name="target.IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="target.IOScan.Priority" Type="UInt">0</Property>
		<Property Name="target.IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="target.IsRemotePanelSupported" Type="Bool">true</Property>
		<Property Name="target.RTCPULoadMonitoringEnabled" Type="Bool">true</Property>
		<Property Name="target.RTDebugWebServerHTTPPort" Type="Int">8001</Property>
		<Property Name="target.RTTarget.ApplicationPath" Type="Path">/home/lvuser/natinst/bin/startup.rtexe</Property>
		<Property Name="target.RTTarget.EnableFileSharing" Type="Bool">true</Property>
		<Property Name="target.RTTarget.IPAccess" Type="Str">+*</Property>
		<Property Name="target.RTTarget.LaunchAppAtBoot" Type="Bool">true</Property>
		<Property Name="target.RTTarget.VIPath" Type="Path">/home/lvuser/natinst/bin</Property>
		<Property Name="target.server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.server.tcp.access" Type="Str">+*</Property>
		<Property Name="target.server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="target.server.tcp.paranoid" Type="Bool">true</Property>
		<Property Name="target.server.tcp.port" Type="Int">3363</Property>
		<Property Name="target.server.tcp.serviceName" Type="Str">Main Application Instance/VI Server</Property>
		<Property Name="target.server.tcp.serviceName.default" Type="Str">Main Application Instance/VI Server</Property>
		<Property Name="target.server.vi.access" Type="Str">+*</Property>
		<Property Name="target.server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="target.server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="target.WebServer.Config" Type="Str">Listen 8000

NI.ServerName default
DocumentRoot "$LVSERVER_DOCROOT"
TypesConfig "$LVSERVER_CONFIGROOT/mime.types"
DirectoryIndex index.htm
WorkerLimit 10
InactivityTimeout 60

LoadModulePath "$LVSERVER_MODULEPATHS"
LoadModule LVAuth lvauthmodule
LoadModule LVRFP lvrfpmodule

#
# Pipeline Definition
#

SetConnector netConnector

AddHandler LVAuth
AddHandler LVRFP

AddHandler fileHandler ""

AddOutputFilter chunkFilter


</Property>
		<Property Name="target.WebServer.Enabled" Type="Bool">false</Property>
		<Property Name="target.WebServer.LogEnabled" Type="Bool">false</Property>
		<Property Name="target.WebServer.LogPath" Type="Path">/c/ni-rt/system/www/www.log</Property>
		<Property Name="target.WebServer.Port" Type="Int">80</Property>
		<Property Name="target.WebServer.RootPath" Type="Path">/c/ni-rt/system/www</Property>
		<Property Name="target.WebServer.TcpAccess" Type="Str">c+*</Property>
		<Property Name="target.WebServer.Timeout" Type="Int">60</Property>
		<Property Name="target.WebServer.ViAccess" Type="Str">+*</Property>
		<Property Name="target.webservices.SecurityAPIKey" Type="Str">PqVr/ifkAQh+lVrdPIykXlFvg12GhhQFR8H9cUhphgg=:pTe9HRlQuMfJxAG6QCGq7UvoUpJzAzWGKy5SbZ+roSU=</Property>
		<Property Name="target.webservices.ValidTimestampWindow" Type="Int">15</Property>
		<Item Name="RT" Type="Folder" URL="../RT">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="Sub VIs" Type="Folder" URL="../Sub VIs">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="TypDefs" Type="Folder" URL="../TypDefs">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="Chassis" Type="cRIO Chassis">
			<Property Name="crio.ProgrammingMode" Type="Str">fpga</Property>
			<Property Name="crio.ResourceID" Type="Str">RIO0</Property>
			<Property Name="crio.Type" Type="Str">cRIO-9063</Property>
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="Real-Time Scan Resources" Type="Module Container">
				<Property Name="crio.ModuleContainerType" Type="Str">crio.RSIModuleContainer</Property>
			</Item>
			<Item Name="FPGA Target" Type="FPGA Target">
				<Property Name="AutoRun" Type="Bool">false</Property>
				<Property Name="configString.guid" Type="Str">{00562116-B562-409C-9D30-D9F98544B9B3}resource=/crio_Mod1/DIO23:16;0;ReadMethodType=u8;WriteMethodType=u8{016B0988-FACD-4648-8FBD-B5999C42C5A9}resource=/crio_Mod2/DI0;0;ReadMethodType=bool{020C6902-295A-42E5-A17E-42A48A30F6DF}resource=/crio_Mod3/DO19;0;ReadMethodType=bool;WriteMethodType=bool{049C7703-D884-47E6-8225-D9BA5491C7BE}resource=/crio_Mod2/DI21;0;ReadMethodType=bool{05BC344C-F549-48A8-A11B-6072DE704315}resource=/crio_Mod1/DIO11;0;ReadMethodType=bool;WriteMethodType=bool{0D68321D-A04D-4330-A91B-D62B6D3AAD37}resource=/crio_Mod3/DO10;0;ReadMethodType=bool;WriteMethodType=bool{0D84AD1B-08FB-4C59-9274-F29D882CEBD1}resource=/crio_Mod3/DO12;0;ReadMethodType=bool;WriteMethodType=bool{0FABB4F2-3ED9-4F31-88BE-7683DC0511F5}resource=/crio_Mod2/DI2;0;ReadMethodType=bool{1026FEBA-ABDE-4063-80AC-E7979C6AD479}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;{10384E08-9379-4DE3-9AFD-FD59E80F5382}resource=/crio_Mod1/DIO31:0;0;ReadMethodType=u32;WriteMethodType=u32{1043F786-C175-4469-B1DE-BBDFE3875AF6}resource=/crio_Mod1/DIO0;0;ReadMethodType=bool;WriteMethodType=bool{17FE2A4B-3E1E-4107-989D-A6B93452E436}resource=/crio_Mod1/DIO22;0;ReadMethodType=bool;WriteMethodType=bool{1A1B4A41-1C62-4082-9D7E-2694353426B3}resource=/crio_Mod2/DI8;0;ReadMethodType=bool{1E8B4F9A-DCB8-4346-BBFF-B5610BF93087}resource=/crio_Mod3/DO22;0;ReadMethodType=bool;WriteMethodType=bool{22CF9362-D74A-428D-9C06-420365B75E48}resource=/crio_Mod2/DI15;0;ReadMethodType=bool{2736EF9F-3FDE-4EC3-B925-B76FE057F29A}resource=/crio_Mod2/DI28;0;ReadMethodType=bool{28CF67CC-658F-434A-BAC2-49DFB1255024}resource=/crio_Mod2/DI15:8;0;ReadMethodType=u8{2F11BDC7-11E0-41C8-A683-B0C490EC9644}resource=/crio_Mod1/DIO15:8;0;ReadMethodType=u8;WriteMethodType=u8{305B9F9D-1BD8-4EFE-A93E-39627D577B09}resource=/crio_Mod3/DO2;0;ReadMethodType=bool;WriteMethodType=bool{3499840D-45F4-4AEF-B32D-4C5DEAEB5594}resource=/System Reset;0;ReadMethodType=bool;WriteMethodType=bool{3573C28A-76EC-43BD-9BB4-F6C69A4C2E0E}resource=/crio_Mod1/DIO4;0;ReadMethodType=bool;WriteMethodType=bool{3775C84B-1696-4C28-9CB1-C6C153CB5FAF}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 1,crio.Type=NI 9403,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.Initial Line Direction=00000000000000000000000000000000,cRIOModule.RsiAttributes=[crioConfig.End]{3920DB94-9852-4D99-B5A2-B91AF7E1F3DA}resource=/crio_Mod2/DI29;0;ReadMethodType=bool{39D01142-1167-4B40-9F6C-326508ACB1D4}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/USER FPGA LED;0;ReadMethodType=bool;WriteMethodType=bool{3D5E2C27-0769-405C-AE48-1728CA31D088}resource=/crio_Mod1/DIO14;0;ReadMethodType=bool;WriteMethodType=bool{3DEC4547-8288-4A8B-85BF-E557F7090CA5}resource=/crio_Mod1/DIO27;0;ReadMethodType=bool;WriteMethodType=bool{3E31F480-7A99-4C40-8547-9FDE5EC4BFA0}resource=/crio_Mod3/DO15:8;0;ReadMethodType=u8;WriteMethodType=u8{41DD390A-A49A-4836-BAB7-D08F2E931A40}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 3,crio.Type=NI 9476,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{444F81C0-89E3-4436-AF38-969D0DDB7EA5}resource=/crio_Mod3/DO13;0;ReadMethodType=bool;WriteMethodType=bool{44D48A2F-DE4E-43B7-A8A0-48AB53C4BE07}resource=/crio_Mod2/DI4;0;ReadMethodType=bool{4507686C-A9EF-4760-872E-38EE1418284C}resource=/crio_Mod3/DO30;0;ReadMethodType=bool;WriteMethodType=bool{45CCAF41-8A26-4109-B0A8-DB5B3E65F598}resource=/crio_Mod3/DO23;0;ReadMethodType=bool;WriteMethodType=bool{4A50D079-2B6B-40CA-981E-5BB68E4E6072}resource=/crio_Mod3/DO8;0;ReadMethodType=bool;WriteMethodType=bool{4BA14884-0303-485D-81D5-57929C7D04A2}resource=/crio_Mod3/DO1;0;ReadMethodType=bool;WriteMethodType=bool{4F020830-0880-45E5-BBA5-C466C65B0BB4}resource=/crio_Mod2/DI22;0;ReadMethodType=bool{503BB1D4-A462-4F7E-8F3A-19AE7ACC8BF7}resource=/crio_Mod1/DIO13;0;ReadMethodType=bool;WriteMethodType=bool{53A9317E-5DC0-42DB-B02F-94872CCF750A}resource=/crio_Mod3/DO18;0;ReadMethodType=bool;WriteMethodType=bool{55370055-7E62-4CA9-8B8A-BA65B3CD81BE}resource=/Scan Clock;0;ReadMethodType=bool{56908C21-85E1-483A-A4A2-A4CD4D211160}resource=/crio_Mod2/DI23;0;ReadMethodType=bool{57004E1C-0A7E-436D-94AF-43971614D169}resource=/crio_Mod1/DIO31;0;ReadMethodType=bool;WriteMethodType=bool{58BC57D1-DA0A-41D8-9CB1-D5762A6F7CBF}resource=/crio_Mod1/DIO1;0;ReadMethodType=bool;WriteMethodType=bool{5CA7854A-B65B-4284-A594-88CA65F1DA70}resource=/crio_Mod1/DIO9;0;ReadMethodType=bool;WriteMethodType=bool{61E4317D-C641-47DF-8A0D-2DF226D62E0C}resource=/crio_Mod2/DI18;0;ReadMethodType=bool{639EC838-091A-4EAC-B912-ED76C3092A75}resource=/crio_Mod1/DIO7;0;ReadMethodType=bool;WriteMethodType=bool{64998FF0-55C1-425A-BC47-95B2A12DFE53}resource=/crio_Mod3/DO7;0;ReadMethodType=bool;WriteMethodType=bool{65FDCC92-2314-4885-8597-0389247683AC}resource=/crio_Mod2/DI14;0;ReadMethodType=bool{66A0CC07-B6D8-4DED-BB95-593C6B7F2417}resource=/crio_Mod2/DI23:16;0;ReadMethodType=u8{66C1D16C-B2CA-493D-BAA1-933236016BAC}resource=/crio_Mod2/DI30;0;ReadMethodType=bool{6B7D89C3-8C7B-48F0-AC09-69C08C302AF2}resource=/crio_Mod3/DO28;0;ReadMethodType=bool;WriteMethodType=bool{6C273585-5129-469A-8E97-BE843C7107E8}resource=/crio_Mod2/DI12;0;ReadMethodType=bool{6E41C8EE-A0D6-416C-985E-DD573E987F95}resource=/crio_Mod1/DIO10;0;ReadMethodType=bool;WriteMethodType=bool{6E874678-081B-4E03-8481-99EFDE2370A6}resource=/crio_Mod3/DO27;0;ReadMethodType=bool;WriteMethodType=bool{6E8B009E-A62B-445F-ADC9-E03128F40A3D}resource=/crio_Mod3/DO26;0;ReadMethodType=bool;WriteMethodType=bool{709E5FB9-7031-4AB5-9026-7B2232D2607E}resource=/crio_Mod1/DIO8;0;ReadMethodType=bool;WriteMethodType=bool{71A38E04-884D-4D5D-9104-308ECFA6F74D}resource=/crio_Mod3/DO31;0;ReadMethodType=bool;WriteMethodType=bool{71A8B016-7C1A-4EA1-B134-3CF7565BB050}resource=/crio_Mod1/DIO23;0;ReadMethodType=bool;WriteMethodType=bool{73E7C557-82F3-44F7-A10F-ABBD306026CC}resource=/crio_Mod1/DIO24;0;ReadMethodType=bool;WriteMethodType=bool{75652323-0379-4CA7-AC0A-4E5D69C21765}resource=/crio_Mod1/DIO12;0;ReadMethodType=bool;WriteMethodType=bool{79EF37B7-14ED-4F3C-8A4A-0E2D23122044}resource=/crio_Mod2/DI25;0;ReadMethodType=bool{7AB082CF-AE39-4FDB-840E-1173396BEB9A}resource=/crio_Mod3/DO21;0;ReadMethodType=bool;WriteMethodType=bool{7FE4A0B6-99DC-4785-A4DC-71239823C6CD}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 2,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{82D6749E-9F51-4098-B498-C578CDB8168F}resource=/crio_Mod3/DO20;0;ReadMethodType=bool;WriteMethodType=bool{83CB0575-DFF0-431D-8F4D-3484E4BA3F16}resource=/crio_Mod3/DO14;0;ReadMethodType=bool;WriteMethodType=bool{86183B77-902B-45CA-91EB-431F5FC9BE6B}resource=/crio_Mod1/DIO7:0;0;ReadMethodType=u8;WriteMethodType=u8{8AB5DED9-5500-4418-8C3A-04EF20B99D45}resource=/crio_Mod3/DO6;0;ReadMethodType=bool;WriteMethodType=bool{8B14372D-20D8-4991-9449-7AFECB3AFFF8}resource=/crio_Mod3/DO4;0;ReadMethodType=bool;WriteMethodType=bool{8D1935D2-A13D-4528-A1A0-BE34FA3AB959}resource=/crio_Mod3/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8{8DC3E634-168C-4EBD-A7A5-05FDDAAD782B}resource=/crio_Mod2/DI5;0;ReadMethodType=bool{8F871B33-CF83-434D-8102-7E3ADBD85E92}resource=/crio_Mod3/DO17;0;ReadMethodType=bool;WriteMethodType=bool{8FAD4D58-5FF4-42DA-B5A6-36DB1E792D0C}resource=/crio_Mod2/DI31:0;0;ReadMethodType=u32{95FEAD71-2493-47B2-BAAC-098CDA44775C}resource=/crio_Mod2/DI16;0;ReadMethodType=bool{9C0ABBBB-5A9C-4199-9197-352FF267DCFA}resource=/crio_Mod3/DO31:0;0;ReadMethodType=u32;WriteMethodType=u32{9E157F47-EAD0-405C-9061-20D3B0B1DEB2}resource=/crio_Mod2/DI20;0;ReadMethodType=bool{9E783D22-2967-4845-B592-BE55FBBBD0AC}resource=/crio_Mod3/DO16;0;ReadMethodType=bool;WriteMethodType=bool{A2425259-5555-4C85-96D3-1B9E1423B694}resource=/crio_Mod1/DIO19;0;ReadMethodType=bool;WriteMethodType=bool{A43CC4DA-D84E-49A1-9FC6-7FADE7FE0C58}resource=/crio_Mod2/DI9;0;ReadMethodType=bool{A62718E0-9738-4D92-8B82-89DA44B88A23}resource=/crio_Mod3/DO25;0;ReadMethodType=bool;WriteMethodType=bool{A656FFF7-2D24-4266-802E-6D256EED7BEE}resource=/crio_Mod2/DI13;0;ReadMethodType=bool{AEEAC7DE-70B0-4006-BBD3-90B734078461}resource=/crio_Mod2/DI11;0;ReadMethodType=bool{B35223B5-0430-4510-A949-BE672B40E6E6}resource=/crio_Mod2/DI27;0;ReadMethodType=bool{B5688A45-C76F-43E5-A99D-19580A6A66A0}resource=/crio_Mod2/DI26;0;ReadMethodType=bool{B7DA2C10-4585-44F3-8D2A-33EBBA04CFC3}resource=/crio_Mod1/DIO21;0;ReadMethodType=bool;WriteMethodType=bool{B86F1B95-4CEA-458A-9584-3DCA4908E079}resource=/crio_Mod2/DI31;0;ReadMethodType=bool{B9CF62AF-44D5-4C24-B58D-9607ECE30699}resource=/crio_Mod1/DIO15;0;ReadMethodType=bool;WriteMethodType=bool{BA05BD40-F9F9-4E0B-98B1-B48D09D8A676}resource=/crio_Mod2/DI24;0;ReadMethodType=bool{BB631661-AF2D-4F67-89E1-4590B39CAA78}resource=/crio_Mod1/DIO20;0;ReadMethodType=bool;WriteMethodType=bool{BBDA328F-FC1D-41A9-9686-96DF871D4641}resource=/Chassis Temperature;0;ReadMethodType=i16{BDCD4479-CB49-4497-B04E-3C430A0276A7}resource=/crio_Mod3/DO23:16;0;ReadMethodType=u8;WriteMethodType=u8{BE2238DD-36CE-41A7-B92C-26BED7784893}resource=/crio_Mod2/DI10;0;ReadMethodType=bool{BECD2180-70B7-4331-B9D7-7448A1065370}resource=/crio_Mod1/DIO28;0;ReadMethodType=bool;WriteMethodType=bool{C218AD58-F228-4954-AA2F-51C4FF56F2E2}resource=/crio_Mod1/DIO25;0;ReadMethodType=bool;WriteMethodType=bool{C7A29B4B-4A3B-4D73-B0CF-7EDA11A6208E}resource=/crio_Mod3/DO15;0;ReadMethodType=bool;WriteMethodType=bool{C84AA5A3-AE33-4F60-ADC0-D29FB88B09BC}resource=/crio_Mod2/DI6;0;ReadMethodType=bool{C9CA73E0-63C3-4C68-9989-E4607F431DFB}resource=/crio_Mod3/DO9;0;ReadMethodType=bool;WriteMethodType=bool{CA8A3767-5563-4B76-8BDD-BF64F2C5E18C}resource=/crio_Mod3/DO29;0;ReadMethodType=bool;WriteMethodType=bool{D1716271-BC52-4CFB-A1CB-4DD319909C94}resource=/crio_Mod1/DIO29;0;ReadMethodType=bool;WriteMethodType=bool{D1A5F91C-4BBF-4F76-A8FC-494DB7A80F1C}resource=/crio_Mod3/DO11;0;ReadMethodType=bool;WriteMethodType=bool{D24632BD-6A46-4F4D-945E-D3B03B760CEC}resource=/crio_Mod1/DIO30;0;ReadMethodType=bool;WriteMethodType=bool{D2739D94-6599-4C66-B0BC-7573C898C1D8}resource=/crio_Mod2/DI19;0;ReadMethodType=bool{D4C820BA-214C-4261-AF39-1E7A4EEBA11B}resource=/crio_Mod2/DI3;0;ReadMethodType=bool{D8251ED7-A8DE-4B1B-B736-9BBD8B70A376}resource=/crio_Mod2/DI7;0;ReadMethodType=bool{DA6B9BAD-0746-462F-8B71-E7DC5EF22156}resource=/crio_Mod2/DI17;0;ReadMethodType=bool{DC69BF39-7E28-482D-9916-8D1BA569C1A3}resource=/crio_Mod1/DIO17;0;ReadMethodType=bool;WriteMethodType=bool{E365D4CC-733A-4E1B-B300-0C3A3EAEF7BF}resource=/crio_Mod2/DI7:0;0;ReadMethodType=u8{E4DAA5DA-423C-4FF9-81AB-8C5D7EC73E1F}resource=/crio_Mod1/DIO2;0;ReadMethodType=bool;WriteMethodType=bool{E835C5A9-29BC-4DC4-B09C-4072DC2F670B}resource=/crio_Mod2/DI31:24;0;ReadMethodType=u8{EC08A3DE-A5D2-42F3-B78C-8150B624146D}resource=/crio_Mod1/DIO31:24;0;ReadMethodType=u8;WriteMethodType=u8{ECB54ED9-35A2-4048-B8A6-FF106DC23343}resource=/crio_Mod1/DIO3;0;ReadMethodType=bool;WriteMethodType=bool{ECC2B02E-CEF2-44C1-972E-63A21806D7CF}resource=/crio_Mod3/DO24;0;ReadMethodType=bool;WriteMethodType=bool{ECE0DC1D-63DC-4D37-BDC3-73C9897BB817}resource=/crio_Mod3/DO5;0;ReadMethodType=bool;WriteMethodType=bool{EE4ECB84-D604-4CF9-9717-A44A2DD5078A}resource=/crio_Mod1/DIO18;0;ReadMethodType=bool;WriteMethodType=bool{EF637D90-A90A-4205-9C34-3A104024B0C9}resource=/crio_Mod3/DO3;0;ReadMethodType=bool;WriteMethodType=bool{F2FD28E8-122B-4FA6-A8AA-17FE9E479C92}resource=/crio_Mod1/DIO6;0;ReadMethodType=bool;WriteMethodType=bool{F344E376-1815-48AF-8AAA-8ED87EDA87EB}resource=/crio_Mod2/DI1;0;ReadMethodType=bool{F46AFDBD-3B43-4103-A682-61502D49D837}resource=/crio_Mod1/DIO26;0;ReadMethodType=bool;WriteMethodType=bool{FC3571D6-DFB9-410B-A19A-900BAE0B19D9}resource=/crio_Mod1/DIO5;0;ReadMethodType=bool;WriteMethodType=bool{FD552EE8-48CA-480F-8812-C02835F0E0FB}resource=/crio_Mod3/DO0;0;ReadMethodType=bool;WriteMethodType=bool{FDE86930-B871-4FD7-B3A5-2A037BE5EC41}resource=/crio_Mod3/DO31:24;0;ReadMethodType=u8;WriteMethodType=u8{FE16717E-17E3-4EAF-83F2-6B6CAD0A0455}resource=/crio_Mod1/DIO16;0;ReadMethodType=bool;WriteMethodType=bool{FF495649-B381-408E-88CA-70310F362A6F}resource=/Sleep;0;ReadMethodType=bool;WriteMethodType=boolcRIO-9063/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9063FPGA_TARGET_FAMILYZYNQTARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
				<Property Name="configString.name" Type="Str">40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;Chassis Temperatureresource=/Chassis Temperature;0;ReadMethodType=i16cRIO-9063/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9063FPGA_TARGET_FAMILYZYNQTARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]I2C_SCLresource=/crio_Mod1/DIO31;0;ReadMethodType=bool;WriteMethodType=boolI2C_SDAresource=/crio_Mod1/DIO30;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO0resource=/crio_Mod1/DIO0;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO10resource=/crio_Mod1/DIO10;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO11resource=/crio_Mod1/DIO11;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO12resource=/crio_Mod1/DIO12;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO13resource=/crio_Mod1/DIO13;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO14resource=/crio_Mod1/DIO14;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO15:8resource=/crio_Mod1/DIO15:8;0;ReadMethodType=u8;WriteMethodType=u8Mod1/DIO15resource=/crio_Mod1/DIO15;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO16resource=/crio_Mod1/DIO16;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO17resource=/crio_Mod1/DIO17;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO18resource=/crio_Mod1/DIO18;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO19resource=/crio_Mod1/DIO19;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO1resource=/crio_Mod1/DIO1;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO20resource=/crio_Mod1/DIO20;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO21resource=/crio_Mod1/DIO21;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO22resource=/crio_Mod1/DIO22;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO23:16resource=/crio_Mod1/DIO23:16;0;ReadMethodType=u8;WriteMethodType=u8Mod1/DIO23resource=/crio_Mod1/DIO23;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO24resource=/crio_Mod1/DIO24;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO25resource=/crio_Mod1/DIO25;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO26resource=/crio_Mod1/DIO26;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO2resource=/crio_Mod1/DIO2;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO31:0resource=/crio_Mod1/DIO31:0;0;ReadMethodType=u32;WriteMethodType=u32Mod1/DIO31:24resource=/crio_Mod1/DIO31:24;0;ReadMethodType=u8;WriteMethodType=u8Mod1/DIO3resource=/crio_Mod1/DIO3;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO4resource=/crio_Mod1/DIO4;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO5resource=/crio_Mod1/DIO5;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO6resource=/crio_Mod1/DIO6;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO7:0resource=/crio_Mod1/DIO7:0;0;ReadMethodType=u8;WriteMethodType=u8Mod1/DIO7resource=/crio_Mod1/DIO7;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO8resource=/crio_Mod1/DIO8;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO9resource=/crio_Mod1/DIO9;0;ReadMethodType=bool;WriteMethodType=boolMod1[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 1,crio.Type=NI 9403,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.Initial Line Direction=00000000000000000000000000000000,cRIOModule.RsiAttributes=[crioConfig.End]Mod2/DI0resource=/crio_Mod2/DI0;0;ReadMethodType=boolMod2/DI10resource=/crio_Mod2/DI10;0;ReadMethodType=boolMod2/DI11resource=/crio_Mod2/DI11;0;ReadMethodType=boolMod2/DI12resource=/crio_Mod2/DI12;0;ReadMethodType=boolMod2/DI13resource=/crio_Mod2/DI13;0;ReadMethodType=boolMod2/DI14resource=/crio_Mod2/DI14;0;ReadMethodType=boolMod2/DI15:8resource=/crio_Mod2/DI15:8;0;ReadMethodType=u8Mod2/DI15resource=/crio_Mod2/DI15;0;ReadMethodType=boolMod2/DI16resource=/crio_Mod2/DI16;0;ReadMethodType=boolMod2/DI17resource=/crio_Mod2/DI17;0;ReadMethodType=boolMod2/DI18resource=/crio_Mod2/DI18;0;ReadMethodType=boolMod2/DI19resource=/crio_Mod2/DI19;0;ReadMethodType=boolMod2/DI1resource=/crio_Mod2/DI1;0;ReadMethodType=boolMod2/DI20resource=/crio_Mod2/DI20;0;ReadMethodType=boolMod2/DI21resource=/crio_Mod2/DI21;0;ReadMethodType=boolMod2/DI22resource=/crio_Mod2/DI22;0;ReadMethodType=boolMod2/DI23:16resource=/crio_Mod2/DI23:16;0;ReadMethodType=u8Mod2/DI23resource=/crio_Mod2/DI23;0;ReadMethodType=boolMod2/DI24resource=/crio_Mod2/DI24;0;ReadMethodType=boolMod2/DI25resource=/crio_Mod2/DI25;0;ReadMethodType=boolMod2/DI26resource=/crio_Mod2/DI26;0;ReadMethodType=boolMod2/DI27resource=/crio_Mod2/DI27;0;ReadMethodType=boolMod2/DI28resource=/crio_Mod2/DI28;0;ReadMethodType=boolMod2/DI29resource=/crio_Mod2/DI29;0;ReadMethodType=boolMod2/DI2resource=/crio_Mod2/DI2;0;ReadMethodType=boolMod2/DI30resource=/crio_Mod2/DI30;0;ReadMethodType=boolMod2/DI31:0resource=/crio_Mod2/DI31:0;0;ReadMethodType=u32Mod2/DI31:24resource=/crio_Mod2/DI31:24;0;ReadMethodType=u8Mod2/DI31resource=/crio_Mod2/DI31;0;ReadMethodType=boolMod2/DI3resource=/crio_Mod2/DI3;0;ReadMethodType=boolMod2/DI4resource=/crio_Mod2/DI4;0;ReadMethodType=boolMod2/DI5resource=/crio_Mod2/DI5;0;ReadMethodType=boolMod2/DI6resource=/crio_Mod2/DI6;0;ReadMethodType=boolMod2/DI7:0resource=/crio_Mod2/DI7:0;0;ReadMethodType=u8Mod2/DI7resource=/crio_Mod2/DI7;0;ReadMethodType=boolMod2/DI8resource=/crio_Mod2/DI8;0;ReadMethodType=boolMod2/DI9resource=/crio_Mod2/DI9;0;ReadMethodType=boolMod2[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 2,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Mod3/DO0resource=/crio_Mod3/DO0;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO10resource=/crio_Mod3/DO10;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO11resource=/crio_Mod3/DO11;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO12resource=/crio_Mod3/DO12;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO13resource=/crio_Mod3/DO13;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO14resource=/crio_Mod3/DO14;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO15:8resource=/crio_Mod3/DO15:8;0;ReadMethodType=u8;WriteMethodType=u8Mod3/DO15resource=/crio_Mod3/DO15;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO16resource=/crio_Mod3/DO16;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO17resource=/crio_Mod3/DO17;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO18resource=/crio_Mod3/DO18;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO19resource=/crio_Mod3/DO19;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO1resource=/crio_Mod3/DO1;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO20resource=/crio_Mod3/DO20;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO21resource=/crio_Mod3/DO21;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO22resource=/crio_Mod3/DO22;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO23:16resource=/crio_Mod3/DO23:16;0;ReadMethodType=u8;WriteMethodType=u8Mod3/DO23resource=/crio_Mod3/DO23;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO24resource=/crio_Mod3/DO24;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO25resource=/crio_Mod3/DO25;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO26resource=/crio_Mod3/DO26;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO27resource=/crio_Mod3/DO27;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO28resource=/crio_Mod3/DO28;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO29resource=/crio_Mod3/DO29;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO2resource=/crio_Mod3/DO2;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO30resource=/crio_Mod3/DO30;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO31:0resource=/crio_Mod3/DO31:0;0;ReadMethodType=u32;WriteMethodType=u32Mod3/DO31:24resource=/crio_Mod3/DO31:24;0;ReadMethodType=u8;WriteMethodType=u8Mod3/DO31resource=/crio_Mod3/DO31;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO3resource=/crio_Mod3/DO3;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO4resource=/crio_Mod3/DO4;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO5resource=/crio_Mod3/DO5;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO6resource=/crio_Mod3/DO6;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO7:0resource=/crio_Mod3/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8Mod3/DO7resource=/crio_Mod3/DO7;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO8resource=/crio_Mod3/DO8;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO9resource=/crio_Mod3/DO9;0;ReadMethodType=bool;WriteMethodType=boolMod3[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 3,crio.Type=NI 9476,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Scan Clockresource=/Scan Clock;0;ReadMethodType=boolSleepresource=/Sleep;0;ReadMethodType=bool;WriteMethodType=boolSPI_CLKresource=/crio_Mod1/DIO27;0;ReadMethodType=bool;WriteMethodType=boolSPI_MISOresource=/crio_Mod1/DIO29;0;ReadMethodType=bool;WriteMethodType=boolSPI_MOSIresource=/crio_Mod1/DIO28;0;ReadMethodType=bool;WriteMethodType=boolSystem Resetresource=/System Reset;0;ReadMethodType=bool;WriteMethodType=boolUSER FPGA LEDArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/USER FPGA LED;0;ReadMethodType=bool;WriteMethodType=bool</Property>
				<Property Name="NI.LV.FPGA.CompileConfigString" Type="Str">cRIO-9063/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9063FPGA_TARGET_FAMILYZYNQTARGET_TYPEFPGA</Property>
				<Property Name="NI.LV.FPGA.Version" Type="Int">6</Property>
				<Property Name="Resource Name" Type="Str">RIO0</Property>
				<Property Name="Target Class" Type="Str">cRIO-9063</Property>
				<Property Name="Top-Level Timing Source" Type="Str">40 MHz Onboard Clock</Property>
				<Property Name="Top-Level Timing Source Is Default" Type="Bool">true</Property>
				<Item Name="Chassis I/O" Type="Folder">
					<Item Name="Chassis Temperature" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/Chassis Temperature</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{BBDA328F-FC1D-41A9-9686-96DF871D4641}</Property>
					</Item>
					<Item Name="Sleep" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/Sleep</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{FF495649-B381-408E-88CA-70310F362A6F}</Property>
					</Item>
					<Item Name="System Reset" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/System Reset</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{3499840D-45F4-4AEF-B32D-4C5DEAEB5594}</Property>
					</Item>
					<Item Name="USER FPGA LED" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="ArbitrationForOutputData">
   <Value>NeverArbitrate</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForOutputData">
   <Value>1</Value>
   </Attribute>
   <Attribute name="NumberOfSyncRegistersForReadInProject">
   <Value>Auto</Value>
   </Attribute>
   <Attribute name="resource">
   <Value>/USER FPGA LED</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{39D01142-1167-4B40-9F6C-326508ACB1D4}</Property>
					</Item>
					<Item Name="Scan Clock" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/Scan Clock</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{55370055-7E62-4CA9-8B8A-BA65B3CD81BE}</Property>
					</Item>
				</Item>
				<Item Name="Mod1" Type="Folder">
					<Item Name="Mod1/DIO0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DIO0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{1043F786-C175-4469-B1DE-BBDFE3875AF6}</Property>
					</Item>
					<Item Name="Mod1/DIO1" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DIO1</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{58BC57D1-DA0A-41D8-9CB1-D5762A6F7CBF}</Property>
					</Item>
					<Item Name="Mod1/DIO2" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DIO2</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{E4DAA5DA-423C-4FF9-81AB-8C5D7EC73E1F}</Property>
					</Item>
					<Item Name="Mod1/DIO3" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DIO3</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{ECB54ED9-35A2-4048-B8A6-FF106DC23343}</Property>
					</Item>
					<Item Name="Mod1/DIO4" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DIO4</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{3573C28A-76EC-43BD-9BB4-F6C69A4C2E0E}</Property>
					</Item>
					<Item Name="Mod1/DIO5" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DIO5</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{FC3571D6-DFB9-410B-A19A-900BAE0B19D9}</Property>
					</Item>
					<Item Name="Mod1/DIO6" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DIO6</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{F2FD28E8-122B-4FA6-A8AA-17FE9E479C92}</Property>
					</Item>
					<Item Name="Mod1/DIO7" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DIO7</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{639EC838-091A-4EAC-B912-ED76C3092A75}</Property>
					</Item>
					<Item Name="Mod1/DIO8" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DIO8</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{709E5FB9-7031-4AB5-9026-7B2232D2607E}</Property>
					</Item>
					<Item Name="Mod1/DIO9" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DIO9</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{5CA7854A-B65B-4284-A594-88CA65F1DA70}</Property>
					</Item>
					<Item Name="Mod1/DIO10" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DIO10</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{6E41C8EE-A0D6-416C-985E-DD573E987F95}</Property>
					</Item>
					<Item Name="Mod1/DIO11" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DIO11</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{05BC344C-F549-48A8-A11B-6072DE704315}</Property>
					</Item>
					<Item Name="Mod1/DIO12" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DIO12</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{75652323-0379-4CA7-AC0A-4E5D69C21765}</Property>
					</Item>
					<Item Name="Mod1/DIO13" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DIO13</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{503BB1D4-A462-4F7E-8F3A-19AE7ACC8BF7}</Property>
					</Item>
					<Item Name="Mod1/DIO14" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DIO14</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{3D5E2C27-0769-405C-AE48-1728CA31D088}</Property>
					</Item>
					<Item Name="Mod1/DIO15" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DIO15</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{B9CF62AF-44D5-4C24-B58D-9607ECE30699}</Property>
					</Item>
					<Item Name="Mod1/DIO16" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DIO16</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{FE16717E-17E3-4EAF-83F2-6B6CAD0A0455}</Property>
					</Item>
					<Item Name="Mod1/DIO17" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DIO17</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{DC69BF39-7E28-482D-9916-8D1BA569C1A3}</Property>
					</Item>
					<Item Name="Mod1/DIO18" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DIO18</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{EE4ECB84-D604-4CF9-9717-A44A2DD5078A}</Property>
					</Item>
					<Item Name="Mod1/DIO19" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DIO19</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{A2425259-5555-4C85-96D3-1B9E1423B694}</Property>
					</Item>
					<Item Name="Mod1/DIO20" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DIO20</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{BB631661-AF2D-4F67-89E1-4590B39CAA78}</Property>
					</Item>
					<Item Name="Mod1/DIO21" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DIO21</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{B7DA2C10-4585-44F3-8D2A-33EBBA04CFC3}</Property>
					</Item>
					<Item Name="Mod1/DIO22" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DIO22</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{17FE2A4B-3E1E-4107-989D-A6B93452E436}</Property>
					</Item>
					<Item Name="Mod1/DIO23" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DIO23</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{71A8B016-7C1A-4EA1-B134-3CF7565BB050}</Property>
					</Item>
					<Item Name="Mod1/DIO24" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DIO24</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{73E7C557-82F3-44F7-A10F-ABBD306026CC}</Property>
					</Item>
					<Item Name="Mod1/DIO25" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DIO25</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{C218AD58-F228-4954-AA2F-51C4FF56F2E2}</Property>
					</Item>
					<Item Name="Mod1/DIO26" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DIO26</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{F46AFDBD-3B43-4103-A682-61502D49D837}</Property>
					</Item>
					<Item Name="SPI_CLK" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DIO27</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{3DEC4547-8288-4A8B-85BF-E557F7090CA5}</Property>
					</Item>
					<Item Name="SPI_MOSI" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DIO28</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{BECD2180-70B7-4331-B9D7-7448A1065370}</Property>
					</Item>
					<Item Name="SPI_MISO" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DIO29</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{D1716271-BC52-4CFB-A1CB-4DD319909C94}</Property>
					</Item>
					<Item Name="I2C_SDA" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DIO30</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{D24632BD-6A46-4F4D-945E-D3B03B760CEC}</Property>
					</Item>
					<Item Name="I2C_SCL" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DIO31</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{57004E1C-0A7E-436D-94AF-43971614D169}</Property>
					</Item>
					<Item Name="Mod1/DIO7:0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DIO7:0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{86183B77-902B-45CA-91EB-431F5FC9BE6B}</Property>
					</Item>
					<Item Name="Mod1/DIO15:8" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DIO15:8</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{2F11BDC7-11E0-41C8-A683-B0C490EC9644}</Property>
					</Item>
					<Item Name="Mod1/DIO23:16" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DIO23:16</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{00562116-B562-409C-9D30-D9F98544B9B3}</Property>
					</Item>
					<Item Name="Mod1/DIO31:24" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DIO31:24</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{EC08A3DE-A5D2-42F3-B78C-8150B624146D}</Property>
					</Item>
					<Item Name="Mod1/DIO31:0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod1/DIO31:0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{10384E08-9379-4DE3-9AFD-FD59E80F5382}</Property>
					</Item>
				</Item>
				<Item Name="Mod2" Type="Folder">
					<Item Name="Mod2/DI0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DI0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{016B0988-FACD-4648-8FBD-B5999C42C5A9}</Property>
					</Item>
					<Item Name="Mod2/DI1" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DI1</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{F344E376-1815-48AF-8AAA-8ED87EDA87EB}</Property>
					</Item>
					<Item Name="Mod2/DI2" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DI2</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{0FABB4F2-3ED9-4F31-88BE-7683DC0511F5}</Property>
					</Item>
					<Item Name="Mod2/DI3" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DI3</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{D4C820BA-214C-4261-AF39-1E7A4EEBA11B}</Property>
					</Item>
					<Item Name="Mod2/DI4" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DI4</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{44D48A2F-DE4E-43B7-A8A0-48AB53C4BE07}</Property>
					</Item>
					<Item Name="Mod2/DI5" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DI5</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{8DC3E634-168C-4EBD-A7A5-05FDDAAD782B}</Property>
					</Item>
					<Item Name="Mod2/DI6" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DI6</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{C84AA5A3-AE33-4F60-ADC0-D29FB88B09BC}</Property>
					</Item>
					<Item Name="Mod2/DI7" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DI7</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{D8251ED7-A8DE-4B1B-B736-9BBD8B70A376}</Property>
					</Item>
					<Item Name="Mod2/DI8" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DI8</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{1A1B4A41-1C62-4082-9D7E-2694353426B3}</Property>
					</Item>
					<Item Name="Mod2/DI9" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DI9</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{A43CC4DA-D84E-49A1-9FC6-7FADE7FE0C58}</Property>
					</Item>
					<Item Name="Mod2/DI10" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DI10</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{BE2238DD-36CE-41A7-B92C-26BED7784893}</Property>
					</Item>
					<Item Name="Mod2/DI11" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DI11</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{AEEAC7DE-70B0-4006-BBD3-90B734078461}</Property>
					</Item>
					<Item Name="Mod2/DI12" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DI12</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{6C273585-5129-469A-8E97-BE843C7107E8}</Property>
					</Item>
					<Item Name="Mod2/DI13" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DI13</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{A656FFF7-2D24-4266-802E-6D256EED7BEE}</Property>
					</Item>
					<Item Name="Mod2/DI14" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DI14</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{65FDCC92-2314-4885-8597-0389247683AC}</Property>
					</Item>
					<Item Name="Mod2/DI15" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DI15</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{22CF9362-D74A-428D-9C06-420365B75E48}</Property>
					</Item>
					<Item Name="Mod2/DI16" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DI16</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{95FEAD71-2493-47B2-BAAC-098CDA44775C}</Property>
					</Item>
					<Item Name="Mod2/DI17" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DI17</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{DA6B9BAD-0746-462F-8B71-E7DC5EF22156}</Property>
					</Item>
					<Item Name="Mod2/DI18" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DI18</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{61E4317D-C641-47DF-8A0D-2DF226D62E0C}</Property>
					</Item>
					<Item Name="Mod2/DI19" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DI19</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{D2739D94-6599-4C66-B0BC-7573C898C1D8}</Property>
					</Item>
					<Item Name="Mod2/DI20" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DI20</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{9E157F47-EAD0-405C-9061-20D3B0B1DEB2}</Property>
					</Item>
					<Item Name="Mod2/DI21" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DI21</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{049C7703-D884-47E6-8225-D9BA5491C7BE}</Property>
					</Item>
					<Item Name="Mod2/DI22" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DI22</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{4F020830-0880-45E5-BBA5-C466C65B0BB4}</Property>
					</Item>
					<Item Name="Mod2/DI23" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DI23</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{56908C21-85E1-483A-A4A2-A4CD4D211160}</Property>
					</Item>
					<Item Name="Mod2/DI24" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DI24</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{BA05BD40-F9F9-4E0B-98B1-B48D09D8A676}</Property>
					</Item>
					<Item Name="Mod2/DI25" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DI25</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{79EF37B7-14ED-4F3C-8A4A-0E2D23122044}</Property>
					</Item>
					<Item Name="Mod2/DI26" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DI26</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{B5688A45-C76F-43E5-A99D-19580A6A66A0}</Property>
					</Item>
					<Item Name="Mod2/DI27" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DI27</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{B35223B5-0430-4510-A949-BE672B40E6E6}</Property>
					</Item>
					<Item Name="Mod2/DI28" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DI28</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{2736EF9F-3FDE-4EC3-B925-B76FE057F29A}</Property>
					</Item>
					<Item Name="Mod2/DI29" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DI29</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{3920DB94-9852-4D99-B5A2-B91AF7E1F3DA}</Property>
					</Item>
					<Item Name="Mod2/DI30" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DI30</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{66C1D16C-B2CA-493D-BAA1-933236016BAC}</Property>
					</Item>
					<Item Name="Mod2/DI31" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DI31</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{B86F1B95-4CEA-458A-9584-3DCA4908E079}</Property>
					</Item>
					<Item Name="Mod2/DI7:0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DI7:0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{E365D4CC-733A-4E1B-B300-0C3A3EAEF7BF}</Property>
					</Item>
					<Item Name="Mod2/DI15:8" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DI15:8</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{28CF67CC-658F-434A-BAC2-49DFB1255024}</Property>
					</Item>
					<Item Name="Mod2/DI23:16" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DI23:16</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{66A0CC07-B6D8-4DED-BB95-593C6B7F2417}</Property>
					</Item>
					<Item Name="Mod2/DI31:24" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DI31:24</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{E835C5A9-29BC-4DC4-B09C-4072DC2F670B}</Property>
					</Item>
					<Item Name="Mod2/DI31:0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod2/DI31:0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{8FAD4D58-5FF4-42DA-B5A6-36DB1E792D0C}</Property>
					</Item>
				</Item>
				<Item Name="Mod3" Type="Folder">
					<Item Name="Mod3/DO0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/DO0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{FD552EE8-48CA-480F-8812-C02835F0E0FB}</Property>
					</Item>
					<Item Name="Mod3/DO1" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/DO1</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{4BA14884-0303-485D-81D5-57929C7D04A2}</Property>
					</Item>
					<Item Name="Mod3/DO2" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/DO2</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{305B9F9D-1BD8-4EFE-A93E-39627D577B09}</Property>
					</Item>
					<Item Name="Mod3/DO3" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/DO3</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{EF637D90-A90A-4205-9C34-3A104024B0C9}</Property>
					</Item>
					<Item Name="Mod3/DO4" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/DO4</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{8B14372D-20D8-4991-9449-7AFECB3AFFF8}</Property>
					</Item>
					<Item Name="Mod3/DO5" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/DO5</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{ECE0DC1D-63DC-4D37-BDC3-73C9897BB817}</Property>
					</Item>
					<Item Name="Mod3/DO6" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/DO6</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{8AB5DED9-5500-4418-8C3A-04EF20B99D45}</Property>
					</Item>
					<Item Name="Mod3/DO7" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/DO7</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{64998FF0-55C1-425A-BC47-95B2A12DFE53}</Property>
					</Item>
					<Item Name="Mod3/DO8" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/DO8</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{4A50D079-2B6B-40CA-981E-5BB68E4E6072}</Property>
					</Item>
					<Item Name="Mod3/DO9" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/DO9</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{C9CA73E0-63C3-4C68-9989-E4607F431DFB}</Property>
					</Item>
					<Item Name="Mod3/DO10" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/DO10</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{0D68321D-A04D-4330-A91B-D62B6D3AAD37}</Property>
					</Item>
					<Item Name="Mod3/DO11" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/DO11</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{D1A5F91C-4BBF-4F76-A8FC-494DB7A80F1C}</Property>
					</Item>
					<Item Name="Mod3/DO12" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/DO12</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{0D84AD1B-08FB-4C59-9274-F29D882CEBD1}</Property>
					</Item>
					<Item Name="Mod3/DO13" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/DO13</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{444F81C0-89E3-4436-AF38-969D0DDB7EA5}</Property>
					</Item>
					<Item Name="Mod3/DO14" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/DO14</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{83CB0575-DFF0-431D-8F4D-3484E4BA3F16}</Property>
					</Item>
					<Item Name="Mod3/DO15" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/DO15</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{C7A29B4B-4A3B-4D73-B0CF-7EDA11A6208E}</Property>
					</Item>
					<Item Name="Mod3/DO16" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/DO16</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{9E783D22-2967-4845-B592-BE55FBBBD0AC}</Property>
					</Item>
					<Item Name="Mod3/DO17" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/DO17</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{8F871B33-CF83-434D-8102-7E3ADBD85E92}</Property>
					</Item>
					<Item Name="Mod3/DO18" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/DO18</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{53A9317E-5DC0-42DB-B02F-94872CCF750A}</Property>
					</Item>
					<Item Name="Mod3/DO19" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/DO19</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{020C6902-295A-42E5-A17E-42A48A30F6DF}</Property>
					</Item>
					<Item Name="Mod3/DO20" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/DO20</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{82D6749E-9F51-4098-B498-C578CDB8168F}</Property>
					</Item>
					<Item Name="Mod3/DO21" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/DO21</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{7AB082CF-AE39-4FDB-840E-1173396BEB9A}</Property>
					</Item>
					<Item Name="Mod3/DO22" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/DO22</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{1E8B4F9A-DCB8-4346-BBFF-B5610BF93087}</Property>
					</Item>
					<Item Name="Mod3/DO23" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/DO23</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{45CCAF41-8A26-4109-B0A8-DB5B3E65F598}</Property>
					</Item>
					<Item Name="Mod3/DO24" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/DO24</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{ECC2B02E-CEF2-44C1-972E-63A21806D7CF}</Property>
					</Item>
					<Item Name="Mod3/DO25" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/DO25</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{A62718E0-9738-4D92-8B82-89DA44B88A23}</Property>
					</Item>
					<Item Name="Mod3/DO26" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/DO26</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{6E8B009E-A62B-445F-ADC9-E03128F40A3D}</Property>
					</Item>
					<Item Name="Mod3/DO27" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/DO27</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{6E874678-081B-4E03-8481-99EFDE2370A6}</Property>
					</Item>
					<Item Name="Mod3/DO28" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/DO28</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{6B7D89C3-8C7B-48F0-AC09-69C08C302AF2}</Property>
					</Item>
					<Item Name="Mod3/DO29" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/DO29</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{CA8A3767-5563-4B76-8BDD-BF64F2C5E18C}</Property>
					</Item>
					<Item Name="Mod3/DO30" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/DO30</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{4507686C-A9EF-4760-872E-38EE1418284C}</Property>
					</Item>
					<Item Name="Mod3/DO31" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/DO31</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{71A38E04-884D-4D5D-9104-308ECFA6F74D}</Property>
					</Item>
					<Item Name="Mod3/DO7:0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/DO7:0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{8D1935D2-A13D-4528-A1A0-BE34FA3AB959}</Property>
					</Item>
					<Item Name="Mod3/DO15:8" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/DO15:8</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{3E31F480-7A99-4C40-8547-9FDE5EC4BFA0}</Property>
					</Item>
					<Item Name="Mod3/DO23:16" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/DO23:16</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{BDCD4479-CB49-4497-B04E-3C430A0276A7}</Property>
					</Item>
					<Item Name="Mod3/DO31:24" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/DO31:24</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{FDE86930-B871-4FD7-B3A5-2A037BE5EC41}</Property>
					</Item>
					<Item Name="Mod3/DO31:0" Type="Elemental IO">
						<Property Name="eioAttrBag" Type="Xml"><AttributeSet name="">
   <Attribute name="resource">
   <Value>/crio_Mod3/DO31:0</Value>
   </Attribute>
</AttributeSet>
</Property>
						<Property Name="FPGA.PersistentID" Type="Str">{9C0ABBBB-5A9C-4199-9197-352FF267DCFA}</Property>
					</Item>
				</Item>
				<Item Name="40 MHz Onboard Clock" Type="FPGA Base Clock">
					<Property Name="FPGA.PersistentID" Type="Str">{1026FEBA-ABDE-4063-80AC-E7979C6AD479}</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig" Type="Str">ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.Accuracy" Type="Dbl">100</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.ClockSignalName" Type="Str">Clk40</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.MaxDutyCycle" Type="Dbl">50</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.MaxFrequency" Type="Dbl">40000000</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.MinDutyCycle" Type="Dbl">50</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.MinFrequency" Type="Dbl">40000000</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.NominalFrequency" Type="Dbl">40000000</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.PeakPeriodJitter" Type="Dbl">250</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.ResourceName" Type="Str">40 MHz Onboard Clock</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.SupportAndRequireRuntimeEnableDisable" Type="Bool">false</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.TopSignalConnect" Type="Str">Clk40</Property>
					<Property Name="NI.LV.FPGA.BaseTSConfig.VariableFrequency" Type="Bool">false</Property>
					<Property Name="NI.LV.FPGA.Valid" Type="Bool">true</Property>
					<Property Name="NI.LV.FPGA.Version" Type="Int">5</Property>
				</Item>
				<Item Name="IP Builder" Type="IP Builder Target">
					<Item Name="Dependencies" Type="Dependencies"/>
					<Item Name="Build Specifications" Type="Build"/>
				</Item>
				<Item Name="Mod1" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 1</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SDcounterSlaveChannelMask" Type="Str">0</Property>
					<Property Name="crio.SDCounterSlaveMasterSlot" Type="Str">0</Property>
					<Property Name="crio.SDInputFilter" Type="Str">128</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">true</Property>
					<Property Name="crio.Type" Type="Str">NI 9403</Property>
					<Property Name="cRIOModule.DigitalIOMode" Type="Str">0</Property>
					<Property Name="cRIOModule.DisableArbitration" Type="Str">false</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="cRIOModule.Initial Line Direction" Type="Str">00000000000000000000000000000000</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{3775C84B-1696-4C28-9CB1-C6C153CB5FAF}</Property>
				</Item>
				<Item Name="Mod3" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 3</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SDcounterSlaveChannelMask" Type="Str">0</Property>
					<Property Name="crio.SDCounterSlaveMasterSlot" Type="Str">0</Property>
					<Property Name="crio.SDInputFilter" Type="Str">128</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
					<Property Name="crio.Type" Type="Str">NI 9476</Property>
					<Property Name="cRIOModule.DigitalIOMode" Type="Str">0</Property>
					<Property Name="cRIOModule.DisableArbitration" Type="Str">false</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{41DD390A-A49A-4836-BAB7-D08F2E931A40}</Property>
				</Item>
				<Item Name="Mod2" Type="RIO C Series Module">
					<Property Name="crio.Calibration" Type="Str">1</Property>
					<Property Name="crio.Location" Type="Str">Slot 2</Property>
					<Property Name="crio.RequiresValidation" Type="Bool">false</Property>
					<Property Name="crio.SDcounterSlaveChannelMask" Type="Str">0</Property>
					<Property Name="crio.SDCounterSlaveMasterSlot" Type="Str">0</Property>
					<Property Name="crio.SDInputFilter" Type="Str">128</Property>
					<Property Name="crio.SupportsDynamicRes" Type="Bool">false</Property>
					<Property Name="crio.Type" Type="Str">NI 9425</Property>
					<Property Name="cRIOModule.DigitalIOMode" Type="Str">0</Property>
					<Property Name="cRIOModule.EnableSpecialtyDigital" Type="Str">false</Property>
					<Property Name="FPGA.PersistentID" Type="Str">{7FE4A0B6-99DC-4785-A4DC-71239823C6CD}</Property>
				</Item>
				<Item Name="FPGA_Main.vi" Type="VI" URL="../FPGA/FPGA_Main.vi">
					<Property Name="BuildSpec" Type="Str">{0B7B2A7E-F652-4E11-8E09-EEC22C6BFA32}</Property>
					<Property Name="configString.guid" Type="Str">{00562116-B562-409C-9D30-D9F98544B9B3}resource=/crio_Mod1/DIO23:16;0;ReadMethodType=u8;WriteMethodType=u8{016B0988-FACD-4648-8FBD-B5999C42C5A9}resource=/crio_Mod2/DI0;0;ReadMethodType=bool{020C6902-295A-42E5-A17E-42A48A30F6DF}resource=/crio_Mod3/DO19;0;ReadMethodType=bool;WriteMethodType=bool{049C7703-D884-47E6-8225-D9BA5491C7BE}resource=/crio_Mod2/DI21;0;ReadMethodType=bool{05BC344C-F549-48A8-A11B-6072DE704315}resource=/crio_Mod1/DIO11;0;ReadMethodType=bool;WriteMethodType=bool{0D68321D-A04D-4330-A91B-D62B6D3AAD37}resource=/crio_Mod3/DO10;0;ReadMethodType=bool;WriteMethodType=bool{0D84AD1B-08FB-4C59-9274-F29D882CEBD1}resource=/crio_Mod3/DO12;0;ReadMethodType=bool;WriteMethodType=bool{0FABB4F2-3ED9-4F31-88BE-7683DC0511F5}resource=/crio_Mod2/DI2;0;ReadMethodType=bool{1026FEBA-ABDE-4063-80AC-E7979C6AD479}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;{10384E08-9379-4DE3-9AFD-FD59E80F5382}resource=/crio_Mod1/DIO31:0;0;ReadMethodType=u32;WriteMethodType=u32{1043F786-C175-4469-B1DE-BBDFE3875AF6}resource=/crio_Mod1/DIO0;0;ReadMethodType=bool;WriteMethodType=bool{17FE2A4B-3E1E-4107-989D-A6B93452E436}resource=/crio_Mod1/DIO22;0;ReadMethodType=bool;WriteMethodType=bool{1A1B4A41-1C62-4082-9D7E-2694353426B3}resource=/crio_Mod2/DI8;0;ReadMethodType=bool{1E8B4F9A-DCB8-4346-BBFF-B5610BF93087}resource=/crio_Mod3/DO22;0;ReadMethodType=bool;WriteMethodType=bool{22CF9362-D74A-428D-9C06-420365B75E48}resource=/crio_Mod2/DI15;0;ReadMethodType=bool{2736EF9F-3FDE-4EC3-B925-B76FE057F29A}resource=/crio_Mod2/DI28;0;ReadMethodType=bool{28CF67CC-658F-434A-BAC2-49DFB1255024}resource=/crio_Mod2/DI15:8;0;ReadMethodType=u8{2F11BDC7-11E0-41C8-A683-B0C490EC9644}resource=/crio_Mod1/DIO15:8;0;ReadMethodType=u8;WriteMethodType=u8{305B9F9D-1BD8-4EFE-A93E-39627D577B09}resource=/crio_Mod3/DO2;0;ReadMethodType=bool;WriteMethodType=bool{3499840D-45F4-4AEF-B32D-4C5DEAEB5594}resource=/System Reset;0;ReadMethodType=bool;WriteMethodType=bool{3573C28A-76EC-43BD-9BB4-F6C69A4C2E0E}resource=/crio_Mod1/DIO4;0;ReadMethodType=bool;WriteMethodType=bool{3775C84B-1696-4C28-9CB1-C6C153CB5FAF}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 1,crio.Type=NI 9403,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.Initial Line Direction=00000000000000000000000000000000,cRIOModule.RsiAttributes=[crioConfig.End]{3920DB94-9852-4D99-B5A2-B91AF7E1F3DA}resource=/crio_Mod2/DI29;0;ReadMethodType=bool{39D01142-1167-4B40-9F6C-326508ACB1D4}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/USER FPGA LED;0;ReadMethodType=bool;WriteMethodType=bool{3D5E2C27-0769-405C-AE48-1728CA31D088}resource=/crio_Mod1/DIO14;0;ReadMethodType=bool;WriteMethodType=bool{3DEC4547-8288-4A8B-85BF-E557F7090CA5}resource=/crio_Mod1/DIO27;0;ReadMethodType=bool;WriteMethodType=bool{3E31F480-7A99-4C40-8547-9FDE5EC4BFA0}resource=/crio_Mod3/DO15:8;0;ReadMethodType=u8;WriteMethodType=u8{41DD390A-A49A-4836-BAB7-D08F2E931A40}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 3,crio.Type=NI 9476,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{444F81C0-89E3-4436-AF38-969D0DDB7EA5}resource=/crio_Mod3/DO13;0;ReadMethodType=bool;WriteMethodType=bool{44D48A2F-DE4E-43B7-A8A0-48AB53C4BE07}resource=/crio_Mod2/DI4;0;ReadMethodType=bool{4507686C-A9EF-4760-872E-38EE1418284C}resource=/crio_Mod3/DO30;0;ReadMethodType=bool;WriteMethodType=bool{45CCAF41-8A26-4109-B0A8-DB5B3E65F598}resource=/crio_Mod3/DO23;0;ReadMethodType=bool;WriteMethodType=bool{4A50D079-2B6B-40CA-981E-5BB68E4E6072}resource=/crio_Mod3/DO8;0;ReadMethodType=bool;WriteMethodType=bool{4BA14884-0303-485D-81D5-57929C7D04A2}resource=/crio_Mod3/DO1;0;ReadMethodType=bool;WriteMethodType=bool{4F020830-0880-45E5-BBA5-C466C65B0BB4}resource=/crio_Mod2/DI22;0;ReadMethodType=bool{503BB1D4-A462-4F7E-8F3A-19AE7ACC8BF7}resource=/crio_Mod1/DIO13;0;ReadMethodType=bool;WriteMethodType=bool{53A9317E-5DC0-42DB-B02F-94872CCF750A}resource=/crio_Mod3/DO18;0;ReadMethodType=bool;WriteMethodType=bool{55370055-7E62-4CA9-8B8A-BA65B3CD81BE}resource=/Scan Clock;0;ReadMethodType=bool{56908C21-85E1-483A-A4A2-A4CD4D211160}resource=/crio_Mod2/DI23;0;ReadMethodType=bool{57004E1C-0A7E-436D-94AF-43971614D169}resource=/crio_Mod1/DIO31;0;ReadMethodType=bool;WriteMethodType=bool{58BC57D1-DA0A-41D8-9CB1-D5762A6F7CBF}resource=/crio_Mod1/DIO1;0;ReadMethodType=bool;WriteMethodType=bool{5CA7854A-B65B-4284-A594-88CA65F1DA70}resource=/crio_Mod1/DIO9;0;ReadMethodType=bool;WriteMethodType=bool{61E4317D-C641-47DF-8A0D-2DF226D62E0C}resource=/crio_Mod2/DI18;0;ReadMethodType=bool{639EC838-091A-4EAC-B912-ED76C3092A75}resource=/crio_Mod1/DIO7;0;ReadMethodType=bool;WriteMethodType=bool{64998FF0-55C1-425A-BC47-95B2A12DFE53}resource=/crio_Mod3/DO7;0;ReadMethodType=bool;WriteMethodType=bool{65FDCC92-2314-4885-8597-0389247683AC}resource=/crio_Mod2/DI14;0;ReadMethodType=bool{66A0CC07-B6D8-4DED-BB95-593C6B7F2417}resource=/crio_Mod2/DI23:16;0;ReadMethodType=u8{66C1D16C-B2CA-493D-BAA1-933236016BAC}resource=/crio_Mod2/DI30;0;ReadMethodType=bool{6B7D89C3-8C7B-48F0-AC09-69C08C302AF2}resource=/crio_Mod3/DO28;0;ReadMethodType=bool;WriteMethodType=bool{6C273585-5129-469A-8E97-BE843C7107E8}resource=/crio_Mod2/DI12;0;ReadMethodType=bool{6E41C8EE-A0D6-416C-985E-DD573E987F95}resource=/crio_Mod1/DIO10;0;ReadMethodType=bool;WriteMethodType=bool{6E874678-081B-4E03-8481-99EFDE2370A6}resource=/crio_Mod3/DO27;0;ReadMethodType=bool;WriteMethodType=bool{6E8B009E-A62B-445F-ADC9-E03128F40A3D}resource=/crio_Mod3/DO26;0;ReadMethodType=bool;WriteMethodType=bool{709E5FB9-7031-4AB5-9026-7B2232D2607E}resource=/crio_Mod1/DIO8;0;ReadMethodType=bool;WriteMethodType=bool{71A38E04-884D-4D5D-9104-308ECFA6F74D}resource=/crio_Mod3/DO31;0;ReadMethodType=bool;WriteMethodType=bool{71A8B016-7C1A-4EA1-B134-3CF7565BB050}resource=/crio_Mod1/DIO23;0;ReadMethodType=bool;WriteMethodType=bool{73E7C557-82F3-44F7-A10F-ABBD306026CC}resource=/crio_Mod1/DIO24;0;ReadMethodType=bool;WriteMethodType=bool{75652323-0379-4CA7-AC0A-4E5D69C21765}resource=/crio_Mod1/DIO12;0;ReadMethodType=bool;WriteMethodType=bool{79EF37B7-14ED-4F3C-8A4A-0E2D23122044}resource=/crio_Mod2/DI25;0;ReadMethodType=bool{7AB082CF-AE39-4FDB-840E-1173396BEB9A}resource=/crio_Mod3/DO21;0;ReadMethodType=bool;WriteMethodType=bool{7FE4A0B6-99DC-4785-A4DC-71239823C6CD}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 2,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{82D6749E-9F51-4098-B498-C578CDB8168F}resource=/crio_Mod3/DO20;0;ReadMethodType=bool;WriteMethodType=bool{83CB0575-DFF0-431D-8F4D-3484E4BA3F16}resource=/crio_Mod3/DO14;0;ReadMethodType=bool;WriteMethodType=bool{86183B77-902B-45CA-91EB-431F5FC9BE6B}resource=/crio_Mod1/DIO7:0;0;ReadMethodType=u8;WriteMethodType=u8{8AB5DED9-5500-4418-8C3A-04EF20B99D45}resource=/crio_Mod3/DO6;0;ReadMethodType=bool;WriteMethodType=bool{8B14372D-20D8-4991-9449-7AFECB3AFFF8}resource=/crio_Mod3/DO4;0;ReadMethodType=bool;WriteMethodType=bool{8D1935D2-A13D-4528-A1A0-BE34FA3AB959}resource=/crio_Mod3/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8{8DC3E634-168C-4EBD-A7A5-05FDDAAD782B}resource=/crio_Mod2/DI5;0;ReadMethodType=bool{8F871B33-CF83-434D-8102-7E3ADBD85E92}resource=/crio_Mod3/DO17;0;ReadMethodType=bool;WriteMethodType=bool{8FAD4D58-5FF4-42DA-B5A6-36DB1E792D0C}resource=/crio_Mod2/DI31:0;0;ReadMethodType=u32{95FEAD71-2493-47B2-BAAC-098CDA44775C}resource=/crio_Mod2/DI16;0;ReadMethodType=bool{9C0ABBBB-5A9C-4199-9197-352FF267DCFA}resource=/crio_Mod3/DO31:0;0;ReadMethodType=u32;WriteMethodType=u32{9E157F47-EAD0-405C-9061-20D3B0B1DEB2}resource=/crio_Mod2/DI20;0;ReadMethodType=bool{9E783D22-2967-4845-B592-BE55FBBBD0AC}resource=/crio_Mod3/DO16;0;ReadMethodType=bool;WriteMethodType=bool{A2425259-5555-4C85-96D3-1B9E1423B694}resource=/crio_Mod1/DIO19;0;ReadMethodType=bool;WriteMethodType=bool{A43CC4DA-D84E-49A1-9FC6-7FADE7FE0C58}resource=/crio_Mod2/DI9;0;ReadMethodType=bool{A62718E0-9738-4D92-8B82-89DA44B88A23}resource=/crio_Mod3/DO25;0;ReadMethodType=bool;WriteMethodType=bool{A656FFF7-2D24-4266-802E-6D256EED7BEE}resource=/crio_Mod2/DI13;0;ReadMethodType=bool{AEEAC7DE-70B0-4006-BBD3-90B734078461}resource=/crio_Mod2/DI11;0;ReadMethodType=bool{B35223B5-0430-4510-A949-BE672B40E6E6}resource=/crio_Mod2/DI27;0;ReadMethodType=bool{B5688A45-C76F-43E5-A99D-19580A6A66A0}resource=/crio_Mod2/DI26;0;ReadMethodType=bool{B7DA2C10-4585-44F3-8D2A-33EBBA04CFC3}resource=/crio_Mod1/DIO21;0;ReadMethodType=bool;WriteMethodType=bool{B86F1B95-4CEA-458A-9584-3DCA4908E079}resource=/crio_Mod2/DI31;0;ReadMethodType=bool{B9CF62AF-44D5-4C24-B58D-9607ECE30699}resource=/crio_Mod1/DIO15;0;ReadMethodType=bool;WriteMethodType=bool{BA05BD40-F9F9-4E0B-98B1-B48D09D8A676}resource=/crio_Mod2/DI24;0;ReadMethodType=bool{BB631661-AF2D-4F67-89E1-4590B39CAA78}resource=/crio_Mod1/DIO20;0;ReadMethodType=bool;WriteMethodType=bool{BBDA328F-FC1D-41A9-9686-96DF871D4641}resource=/Chassis Temperature;0;ReadMethodType=i16{BDCD4479-CB49-4497-B04E-3C430A0276A7}resource=/crio_Mod3/DO23:16;0;ReadMethodType=u8;WriteMethodType=u8{BE2238DD-36CE-41A7-B92C-26BED7784893}resource=/crio_Mod2/DI10;0;ReadMethodType=bool{BECD2180-70B7-4331-B9D7-7448A1065370}resource=/crio_Mod1/DIO28;0;ReadMethodType=bool;WriteMethodType=bool{C218AD58-F228-4954-AA2F-51C4FF56F2E2}resource=/crio_Mod1/DIO25;0;ReadMethodType=bool;WriteMethodType=bool{C7A29B4B-4A3B-4D73-B0CF-7EDA11A6208E}resource=/crio_Mod3/DO15;0;ReadMethodType=bool;WriteMethodType=bool{C84AA5A3-AE33-4F60-ADC0-D29FB88B09BC}resource=/crio_Mod2/DI6;0;ReadMethodType=bool{C9CA73E0-63C3-4C68-9989-E4607F431DFB}resource=/crio_Mod3/DO9;0;ReadMethodType=bool;WriteMethodType=bool{CA8A3767-5563-4B76-8BDD-BF64F2C5E18C}resource=/crio_Mod3/DO29;0;ReadMethodType=bool;WriteMethodType=bool{D1716271-BC52-4CFB-A1CB-4DD319909C94}resource=/crio_Mod1/DIO29;0;ReadMethodType=bool;WriteMethodType=bool{D1A5F91C-4BBF-4F76-A8FC-494DB7A80F1C}resource=/crio_Mod3/DO11;0;ReadMethodType=bool;WriteMethodType=bool{D24632BD-6A46-4F4D-945E-D3B03B760CEC}resource=/crio_Mod1/DIO30;0;ReadMethodType=bool;WriteMethodType=bool{D2739D94-6599-4C66-B0BC-7573C898C1D8}resource=/crio_Mod2/DI19;0;ReadMethodType=bool{D4C820BA-214C-4261-AF39-1E7A4EEBA11B}resource=/crio_Mod2/DI3;0;ReadMethodType=bool{D8251ED7-A8DE-4B1B-B736-9BBD8B70A376}resource=/crio_Mod2/DI7;0;ReadMethodType=bool{DA6B9BAD-0746-462F-8B71-E7DC5EF22156}resource=/crio_Mod2/DI17;0;ReadMethodType=bool{DC69BF39-7E28-482D-9916-8D1BA569C1A3}resource=/crio_Mod1/DIO17;0;ReadMethodType=bool;WriteMethodType=bool{E365D4CC-733A-4E1B-B300-0C3A3EAEF7BF}resource=/crio_Mod2/DI7:0;0;ReadMethodType=u8{E4DAA5DA-423C-4FF9-81AB-8C5D7EC73E1F}resource=/crio_Mod1/DIO2;0;ReadMethodType=bool;WriteMethodType=bool{E835C5A9-29BC-4DC4-B09C-4072DC2F670B}resource=/crio_Mod2/DI31:24;0;ReadMethodType=u8{EC08A3DE-A5D2-42F3-B78C-8150B624146D}resource=/crio_Mod1/DIO31:24;0;ReadMethodType=u8;WriteMethodType=u8{ECB54ED9-35A2-4048-B8A6-FF106DC23343}resource=/crio_Mod1/DIO3;0;ReadMethodType=bool;WriteMethodType=bool{ECC2B02E-CEF2-44C1-972E-63A21806D7CF}resource=/crio_Mod3/DO24;0;ReadMethodType=bool;WriteMethodType=bool{ECE0DC1D-63DC-4D37-BDC3-73C9897BB817}resource=/crio_Mod3/DO5;0;ReadMethodType=bool;WriteMethodType=bool{EE4ECB84-D604-4CF9-9717-A44A2DD5078A}resource=/crio_Mod1/DIO18;0;ReadMethodType=bool;WriteMethodType=bool{EF637D90-A90A-4205-9C34-3A104024B0C9}resource=/crio_Mod3/DO3;0;ReadMethodType=bool;WriteMethodType=bool{F2FD28E8-122B-4FA6-A8AA-17FE9E479C92}resource=/crio_Mod1/DIO6;0;ReadMethodType=bool;WriteMethodType=bool{F344E376-1815-48AF-8AAA-8ED87EDA87EB}resource=/crio_Mod2/DI1;0;ReadMethodType=bool{F46AFDBD-3B43-4103-A682-61502D49D837}resource=/crio_Mod1/DIO26;0;ReadMethodType=bool;WriteMethodType=bool{FC3571D6-DFB9-410B-A19A-900BAE0B19D9}resource=/crio_Mod1/DIO5;0;ReadMethodType=bool;WriteMethodType=bool{FD552EE8-48CA-480F-8812-C02835F0E0FB}resource=/crio_Mod3/DO0;0;ReadMethodType=bool;WriteMethodType=bool{FDE86930-B871-4FD7-B3A5-2A037BE5EC41}resource=/crio_Mod3/DO31:24;0;ReadMethodType=u8;WriteMethodType=u8{FE16717E-17E3-4EAF-83F2-6B6CAD0A0455}resource=/crio_Mod1/DIO16;0;ReadMethodType=bool;WriteMethodType=bool{FF495649-B381-408E-88CA-70310F362A6F}resource=/Sleep;0;ReadMethodType=bool;WriteMethodType=boolcRIO-9063/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9063FPGA_TARGET_FAMILYZYNQTARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
					<Property Name="configString.name" Type="Str">40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;Chassis Temperatureresource=/Chassis Temperature;0;ReadMethodType=i16cRIO-9063/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9063FPGA_TARGET_FAMILYZYNQTARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]I2C_SCLresource=/crio_Mod1/DIO31;0;ReadMethodType=bool;WriteMethodType=boolI2C_SDAresource=/crio_Mod1/DIO30;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO0resource=/crio_Mod1/DIO0;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO10resource=/crio_Mod1/DIO10;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO11resource=/crio_Mod1/DIO11;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO12resource=/crio_Mod1/DIO12;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO13resource=/crio_Mod1/DIO13;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO14resource=/crio_Mod1/DIO14;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO15:8resource=/crio_Mod1/DIO15:8;0;ReadMethodType=u8;WriteMethodType=u8Mod1/DIO15resource=/crio_Mod1/DIO15;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO16resource=/crio_Mod1/DIO16;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO17resource=/crio_Mod1/DIO17;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO18resource=/crio_Mod1/DIO18;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO19resource=/crio_Mod1/DIO19;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO1resource=/crio_Mod1/DIO1;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO20resource=/crio_Mod1/DIO20;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO21resource=/crio_Mod1/DIO21;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO22resource=/crio_Mod1/DIO22;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO23:16resource=/crio_Mod1/DIO23:16;0;ReadMethodType=u8;WriteMethodType=u8Mod1/DIO23resource=/crio_Mod1/DIO23;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO24resource=/crio_Mod1/DIO24;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO25resource=/crio_Mod1/DIO25;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO26resource=/crio_Mod1/DIO26;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO2resource=/crio_Mod1/DIO2;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO31:0resource=/crio_Mod1/DIO31:0;0;ReadMethodType=u32;WriteMethodType=u32Mod1/DIO31:24resource=/crio_Mod1/DIO31:24;0;ReadMethodType=u8;WriteMethodType=u8Mod1/DIO3resource=/crio_Mod1/DIO3;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO4resource=/crio_Mod1/DIO4;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO5resource=/crio_Mod1/DIO5;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO6resource=/crio_Mod1/DIO6;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO7:0resource=/crio_Mod1/DIO7:0;0;ReadMethodType=u8;WriteMethodType=u8Mod1/DIO7resource=/crio_Mod1/DIO7;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO8resource=/crio_Mod1/DIO8;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO9resource=/crio_Mod1/DIO9;0;ReadMethodType=bool;WriteMethodType=boolMod1[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 1,crio.Type=NI 9403,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.Initial Line Direction=00000000000000000000000000000000,cRIOModule.RsiAttributes=[crioConfig.End]Mod2/DI0resource=/crio_Mod2/DI0;0;ReadMethodType=boolMod2/DI10resource=/crio_Mod2/DI10;0;ReadMethodType=boolMod2/DI11resource=/crio_Mod2/DI11;0;ReadMethodType=boolMod2/DI12resource=/crio_Mod2/DI12;0;ReadMethodType=boolMod2/DI13resource=/crio_Mod2/DI13;0;ReadMethodType=boolMod2/DI14resource=/crio_Mod2/DI14;0;ReadMethodType=boolMod2/DI15:8resource=/crio_Mod2/DI15:8;0;ReadMethodType=u8Mod2/DI15resource=/crio_Mod2/DI15;0;ReadMethodType=boolMod2/DI16resource=/crio_Mod2/DI16;0;ReadMethodType=boolMod2/DI17resource=/crio_Mod2/DI17;0;ReadMethodType=boolMod2/DI18resource=/crio_Mod2/DI18;0;ReadMethodType=boolMod2/DI19resource=/crio_Mod2/DI19;0;ReadMethodType=boolMod2/DI1resource=/crio_Mod2/DI1;0;ReadMethodType=boolMod2/DI20resource=/crio_Mod2/DI20;0;ReadMethodType=boolMod2/DI21resource=/crio_Mod2/DI21;0;ReadMethodType=boolMod2/DI22resource=/crio_Mod2/DI22;0;ReadMethodType=boolMod2/DI23:16resource=/crio_Mod2/DI23:16;0;ReadMethodType=u8Mod2/DI23resource=/crio_Mod2/DI23;0;ReadMethodType=boolMod2/DI24resource=/crio_Mod2/DI24;0;ReadMethodType=boolMod2/DI25resource=/crio_Mod2/DI25;0;ReadMethodType=boolMod2/DI26resource=/crio_Mod2/DI26;0;ReadMethodType=boolMod2/DI27resource=/crio_Mod2/DI27;0;ReadMethodType=boolMod2/DI28resource=/crio_Mod2/DI28;0;ReadMethodType=boolMod2/DI29resource=/crio_Mod2/DI29;0;ReadMethodType=boolMod2/DI2resource=/crio_Mod2/DI2;0;ReadMethodType=boolMod2/DI30resource=/crio_Mod2/DI30;0;ReadMethodType=boolMod2/DI31:0resource=/crio_Mod2/DI31:0;0;ReadMethodType=u32Mod2/DI31:24resource=/crio_Mod2/DI31:24;0;ReadMethodType=u8Mod2/DI31resource=/crio_Mod2/DI31;0;ReadMethodType=boolMod2/DI3resource=/crio_Mod2/DI3;0;ReadMethodType=boolMod2/DI4resource=/crio_Mod2/DI4;0;ReadMethodType=boolMod2/DI5resource=/crio_Mod2/DI5;0;ReadMethodType=boolMod2/DI6resource=/crio_Mod2/DI6;0;ReadMethodType=boolMod2/DI7:0resource=/crio_Mod2/DI7:0;0;ReadMethodType=u8Mod2/DI7resource=/crio_Mod2/DI7;0;ReadMethodType=boolMod2/DI8resource=/crio_Mod2/DI8;0;ReadMethodType=boolMod2/DI9resource=/crio_Mod2/DI9;0;ReadMethodType=boolMod2[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 2,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Mod3/DO0resource=/crio_Mod3/DO0;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO10resource=/crio_Mod3/DO10;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO11resource=/crio_Mod3/DO11;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO12resource=/crio_Mod3/DO12;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO13resource=/crio_Mod3/DO13;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO14resource=/crio_Mod3/DO14;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO15:8resource=/crio_Mod3/DO15:8;0;ReadMethodType=u8;WriteMethodType=u8Mod3/DO15resource=/crio_Mod3/DO15;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO16resource=/crio_Mod3/DO16;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO17resource=/crio_Mod3/DO17;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO18resource=/crio_Mod3/DO18;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO19resource=/crio_Mod3/DO19;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO1resource=/crio_Mod3/DO1;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO20resource=/crio_Mod3/DO20;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO21resource=/crio_Mod3/DO21;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO22resource=/crio_Mod3/DO22;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO23:16resource=/crio_Mod3/DO23:16;0;ReadMethodType=u8;WriteMethodType=u8Mod3/DO23resource=/crio_Mod3/DO23;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO24resource=/crio_Mod3/DO24;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO25resource=/crio_Mod3/DO25;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO26resource=/crio_Mod3/DO26;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO27resource=/crio_Mod3/DO27;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO28resource=/crio_Mod3/DO28;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO29resource=/crio_Mod3/DO29;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO2resource=/crio_Mod3/DO2;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO30resource=/crio_Mod3/DO30;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO31:0resource=/crio_Mod3/DO31:0;0;ReadMethodType=u32;WriteMethodType=u32Mod3/DO31:24resource=/crio_Mod3/DO31:24;0;ReadMethodType=u8;WriteMethodType=u8Mod3/DO31resource=/crio_Mod3/DO31;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO3resource=/crio_Mod3/DO3;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO4resource=/crio_Mod3/DO4;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO5resource=/crio_Mod3/DO5;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO6resource=/crio_Mod3/DO6;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO7:0resource=/crio_Mod3/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8Mod3/DO7resource=/crio_Mod3/DO7;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO8resource=/crio_Mod3/DO8;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO9resource=/crio_Mod3/DO9;0;ReadMethodType=bool;WriteMethodType=boolMod3[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 3,crio.Type=NI 9476,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Scan Clockresource=/Scan Clock;0;ReadMethodType=boolSleepresource=/Sleep;0;ReadMethodType=bool;WriteMethodType=boolSPI_CLKresource=/crio_Mod1/DIO27;0;ReadMethodType=bool;WriteMethodType=boolSPI_MISOresource=/crio_Mod1/DIO29;0;ReadMethodType=bool;WriteMethodType=boolSPI_MOSIresource=/crio_Mod1/DIO28;0;ReadMethodType=bool;WriteMethodType=boolSystem Resetresource=/System Reset;0;ReadMethodType=bool;WriteMethodType=boolUSER FPGA LEDArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/USER FPGA LED;0;ReadMethodType=bool;WriteMethodType=bool</Property>
					<Property Name="NI.LV.FPGA.InterfaceBitfile" Type="Str">D:\SEN_DATAspace\KFT Laird\cRio-9063\FPGA Bitfiles\cRio-9063_FPGATarget_FPGAMain_inOBxH1hPKc.lvbitx</Property>
				</Item>
				<Item Name="FPGA_Main_IBN.vi" Type="VI" URL="../FPGA/FPGA_Main_IBN.vi">
					<Property Name="configString.guid" Type="Str">{00562116-B562-409C-9D30-D9F98544B9B3}resource=/crio_Mod1/DIO23:16;0;ReadMethodType=u8;WriteMethodType=u8{016B0988-FACD-4648-8FBD-B5999C42C5A9}resource=/crio_Mod2/DI0;0;ReadMethodType=bool{020C6902-295A-42E5-A17E-42A48A30F6DF}resource=/crio_Mod3/DO19;0;ReadMethodType=bool;WriteMethodType=bool{049C7703-D884-47E6-8225-D9BA5491C7BE}resource=/crio_Mod2/DI21;0;ReadMethodType=bool{05BC344C-F549-48A8-A11B-6072DE704315}resource=/crio_Mod1/DIO11;0;ReadMethodType=bool;WriteMethodType=bool{0D68321D-A04D-4330-A91B-D62B6D3AAD37}resource=/crio_Mod3/DO10;0;ReadMethodType=bool;WriteMethodType=bool{0D84AD1B-08FB-4C59-9274-F29D882CEBD1}resource=/crio_Mod3/DO12;0;ReadMethodType=bool;WriteMethodType=bool{0FABB4F2-3ED9-4F31-88BE-7683DC0511F5}resource=/crio_Mod2/DI2;0;ReadMethodType=bool{1026FEBA-ABDE-4063-80AC-E7979C6AD479}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;{10384E08-9379-4DE3-9AFD-FD59E80F5382}resource=/crio_Mod1/DIO31:0;0;ReadMethodType=u32;WriteMethodType=u32{1043F786-C175-4469-B1DE-BBDFE3875AF6}resource=/crio_Mod1/DIO0;0;ReadMethodType=bool;WriteMethodType=bool{17FE2A4B-3E1E-4107-989D-A6B93452E436}resource=/crio_Mod1/DIO22;0;ReadMethodType=bool;WriteMethodType=bool{1A1B4A41-1C62-4082-9D7E-2694353426B3}resource=/crio_Mod2/DI8;0;ReadMethodType=bool{1E8B4F9A-DCB8-4346-BBFF-B5610BF93087}resource=/crio_Mod3/DO22;0;ReadMethodType=bool;WriteMethodType=bool{22CF9362-D74A-428D-9C06-420365B75E48}resource=/crio_Mod2/DI15;0;ReadMethodType=bool{2736EF9F-3FDE-4EC3-B925-B76FE057F29A}resource=/crio_Mod2/DI28;0;ReadMethodType=bool{28CF67CC-658F-434A-BAC2-49DFB1255024}resource=/crio_Mod2/DI15:8;0;ReadMethodType=u8{2F11BDC7-11E0-41C8-A683-B0C490EC9644}resource=/crio_Mod1/DIO15:8;0;ReadMethodType=u8;WriteMethodType=u8{305B9F9D-1BD8-4EFE-A93E-39627D577B09}resource=/crio_Mod3/DO2;0;ReadMethodType=bool;WriteMethodType=bool{3499840D-45F4-4AEF-B32D-4C5DEAEB5594}resource=/System Reset;0;ReadMethodType=bool;WriteMethodType=bool{3573C28A-76EC-43BD-9BB4-F6C69A4C2E0E}resource=/crio_Mod1/DIO4;0;ReadMethodType=bool;WriteMethodType=bool{3775C84B-1696-4C28-9CB1-C6C153CB5FAF}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 1,crio.Type=NI 9403,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.Initial Line Direction=00000000000000000000000000000000,cRIOModule.RsiAttributes=[crioConfig.End]{3920DB94-9852-4D99-B5A2-B91AF7E1F3DA}resource=/crio_Mod2/DI29;0;ReadMethodType=bool{39D01142-1167-4B40-9F6C-326508ACB1D4}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/USER FPGA LED;0;ReadMethodType=bool;WriteMethodType=bool{3D5E2C27-0769-405C-AE48-1728CA31D088}resource=/crio_Mod1/DIO14;0;ReadMethodType=bool;WriteMethodType=bool{3DEC4547-8288-4A8B-85BF-E557F7090CA5}resource=/crio_Mod1/DIO27;0;ReadMethodType=bool;WriteMethodType=bool{3E31F480-7A99-4C40-8547-9FDE5EC4BFA0}resource=/crio_Mod3/DO15:8;0;ReadMethodType=u8;WriteMethodType=u8{41DD390A-A49A-4836-BAB7-D08F2E931A40}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 3,crio.Type=NI 9476,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{444F81C0-89E3-4436-AF38-969D0DDB7EA5}resource=/crio_Mod3/DO13;0;ReadMethodType=bool;WriteMethodType=bool{44D48A2F-DE4E-43B7-A8A0-48AB53C4BE07}resource=/crio_Mod2/DI4;0;ReadMethodType=bool{4507686C-A9EF-4760-872E-38EE1418284C}resource=/crio_Mod3/DO30;0;ReadMethodType=bool;WriteMethodType=bool{45CCAF41-8A26-4109-B0A8-DB5B3E65F598}resource=/crio_Mod3/DO23;0;ReadMethodType=bool;WriteMethodType=bool{4A50D079-2B6B-40CA-981E-5BB68E4E6072}resource=/crio_Mod3/DO8;0;ReadMethodType=bool;WriteMethodType=bool{4BA14884-0303-485D-81D5-57929C7D04A2}resource=/crio_Mod3/DO1;0;ReadMethodType=bool;WriteMethodType=bool{4F020830-0880-45E5-BBA5-C466C65B0BB4}resource=/crio_Mod2/DI22;0;ReadMethodType=bool{503BB1D4-A462-4F7E-8F3A-19AE7ACC8BF7}resource=/crio_Mod1/DIO13;0;ReadMethodType=bool;WriteMethodType=bool{53A9317E-5DC0-42DB-B02F-94872CCF750A}resource=/crio_Mod3/DO18;0;ReadMethodType=bool;WriteMethodType=bool{55370055-7E62-4CA9-8B8A-BA65B3CD81BE}resource=/Scan Clock;0;ReadMethodType=bool{56908C21-85E1-483A-A4A2-A4CD4D211160}resource=/crio_Mod2/DI23;0;ReadMethodType=bool{57004E1C-0A7E-436D-94AF-43971614D169}resource=/crio_Mod1/DIO31;0;ReadMethodType=bool;WriteMethodType=bool{58BC57D1-DA0A-41D8-9CB1-D5762A6F7CBF}resource=/crio_Mod1/DIO1;0;ReadMethodType=bool;WriteMethodType=bool{5CA7854A-B65B-4284-A594-88CA65F1DA70}resource=/crio_Mod1/DIO9;0;ReadMethodType=bool;WriteMethodType=bool{61E4317D-C641-47DF-8A0D-2DF226D62E0C}resource=/crio_Mod2/DI18;0;ReadMethodType=bool{639EC838-091A-4EAC-B912-ED76C3092A75}resource=/crio_Mod1/DIO7;0;ReadMethodType=bool;WriteMethodType=bool{64998FF0-55C1-425A-BC47-95B2A12DFE53}resource=/crio_Mod3/DO7;0;ReadMethodType=bool;WriteMethodType=bool{65FDCC92-2314-4885-8597-0389247683AC}resource=/crio_Mod2/DI14;0;ReadMethodType=bool{66A0CC07-B6D8-4DED-BB95-593C6B7F2417}resource=/crio_Mod2/DI23:16;0;ReadMethodType=u8{66C1D16C-B2CA-493D-BAA1-933236016BAC}resource=/crio_Mod2/DI30;0;ReadMethodType=bool{6B7D89C3-8C7B-48F0-AC09-69C08C302AF2}resource=/crio_Mod3/DO28;0;ReadMethodType=bool;WriteMethodType=bool{6C273585-5129-469A-8E97-BE843C7107E8}resource=/crio_Mod2/DI12;0;ReadMethodType=bool{6E41C8EE-A0D6-416C-985E-DD573E987F95}resource=/crio_Mod1/DIO10;0;ReadMethodType=bool;WriteMethodType=bool{6E874678-081B-4E03-8481-99EFDE2370A6}resource=/crio_Mod3/DO27;0;ReadMethodType=bool;WriteMethodType=bool{6E8B009E-A62B-445F-ADC9-E03128F40A3D}resource=/crio_Mod3/DO26;0;ReadMethodType=bool;WriteMethodType=bool{709E5FB9-7031-4AB5-9026-7B2232D2607E}resource=/crio_Mod1/DIO8;0;ReadMethodType=bool;WriteMethodType=bool{71A38E04-884D-4D5D-9104-308ECFA6F74D}resource=/crio_Mod3/DO31;0;ReadMethodType=bool;WriteMethodType=bool{71A8B016-7C1A-4EA1-B134-3CF7565BB050}resource=/crio_Mod1/DIO23;0;ReadMethodType=bool;WriteMethodType=bool{73E7C557-82F3-44F7-A10F-ABBD306026CC}resource=/crio_Mod1/DIO24;0;ReadMethodType=bool;WriteMethodType=bool{75652323-0379-4CA7-AC0A-4E5D69C21765}resource=/crio_Mod1/DIO12;0;ReadMethodType=bool;WriteMethodType=bool{79EF37B7-14ED-4F3C-8A4A-0E2D23122044}resource=/crio_Mod2/DI25;0;ReadMethodType=bool{7AB082CF-AE39-4FDB-840E-1173396BEB9A}resource=/crio_Mod3/DO21;0;ReadMethodType=bool;WriteMethodType=bool{7FE4A0B6-99DC-4785-A4DC-71239823C6CD}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 2,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{82D6749E-9F51-4098-B498-C578CDB8168F}resource=/crio_Mod3/DO20;0;ReadMethodType=bool;WriteMethodType=bool{83CB0575-DFF0-431D-8F4D-3484E4BA3F16}resource=/crio_Mod3/DO14;0;ReadMethodType=bool;WriteMethodType=bool{86183B77-902B-45CA-91EB-431F5FC9BE6B}resource=/crio_Mod1/DIO7:0;0;ReadMethodType=u8;WriteMethodType=u8{8AB5DED9-5500-4418-8C3A-04EF20B99D45}resource=/crio_Mod3/DO6;0;ReadMethodType=bool;WriteMethodType=bool{8B14372D-20D8-4991-9449-7AFECB3AFFF8}resource=/crio_Mod3/DO4;0;ReadMethodType=bool;WriteMethodType=bool{8D1935D2-A13D-4528-A1A0-BE34FA3AB959}resource=/crio_Mod3/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8{8DC3E634-168C-4EBD-A7A5-05FDDAAD782B}resource=/crio_Mod2/DI5;0;ReadMethodType=bool{8F871B33-CF83-434D-8102-7E3ADBD85E92}resource=/crio_Mod3/DO17;0;ReadMethodType=bool;WriteMethodType=bool{8FAD4D58-5FF4-42DA-B5A6-36DB1E792D0C}resource=/crio_Mod2/DI31:0;0;ReadMethodType=u32{95FEAD71-2493-47B2-BAAC-098CDA44775C}resource=/crio_Mod2/DI16;0;ReadMethodType=bool{9C0ABBBB-5A9C-4199-9197-352FF267DCFA}resource=/crio_Mod3/DO31:0;0;ReadMethodType=u32;WriteMethodType=u32{9E157F47-EAD0-405C-9061-20D3B0B1DEB2}resource=/crio_Mod2/DI20;0;ReadMethodType=bool{9E783D22-2967-4845-B592-BE55FBBBD0AC}resource=/crio_Mod3/DO16;0;ReadMethodType=bool;WriteMethodType=bool{A2425259-5555-4C85-96D3-1B9E1423B694}resource=/crio_Mod1/DIO19;0;ReadMethodType=bool;WriteMethodType=bool{A43CC4DA-D84E-49A1-9FC6-7FADE7FE0C58}resource=/crio_Mod2/DI9;0;ReadMethodType=bool{A62718E0-9738-4D92-8B82-89DA44B88A23}resource=/crio_Mod3/DO25;0;ReadMethodType=bool;WriteMethodType=bool{A656FFF7-2D24-4266-802E-6D256EED7BEE}resource=/crio_Mod2/DI13;0;ReadMethodType=bool{AEEAC7DE-70B0-4006-BBD3-90B734078461}resource=/crio_Mod2/DI11;0;ReadMethodType=bool{B35223B5-0430-4510-A949-BE672B40E6E6}resource=/crio_Mod2/DI27;0;ReadMethodType=bool{B5688A45-C76F-43E5-A99D-19580A6A66A0}resource=/crio_Mod2/DI26;0;ReadMethodType=bool{B7DA2C10-4585-44F3-8D2A-33EBBA04CFC3}resource=/crio_Mod1/DIO21;0;ReadMethodType=bool;WriteMethodType=bool{B86F1B95-4CEA-458A-9584-3DCA4908E079}resource=/crio_Mod2/DI31;0;ReadMethodType=bool{B9CF62AF-44D5-4C24-B58D-9607ECE30699}resource=/crio_Mod1/DIO15;0;ReadMethodType=bool;WriteMethodType=bool{BA05BD40-F9F9-4E0B-98B1-B48D09D8A676}resource=/crio_Mod2/DI24;0;ReadMethodType=bool{BB631661-AF2D-4F67-89E1-4590B39CAA78}resource=/crio_Mod1/DIO20;0;ReadMethodType=bool;WriteMethodType=bool{BBDA328F-FC1D-41A9-9686-96DF871D4641}resource=/Chassis Temperature;0;ReadMethodType=i16{BDCD4479-CB49-4497-B04E-3C430A0276A7}resource=/crio_Mod3/DO23:16;0;ReadMethodType=u8;WriteMethodType=u8{BE2238DD-36CE-41A7-B92C-26BED7784893}resource=/crio_Mod2/DI10;0;ReadMethodType=bool{BECD2180-70B7-4331-B9D7-7448A1065370}resource=/crio_Mod1/DIO28;0;ReadMethodType=bool;WriteMethodType=bool{C218AD58-F228-4954-AA2F-51C4FF56F2E2}resource=/crio_Mod1/DIO25;0;ReadMethodType=bool;WriteMethodType=bool{C7A29B4B-4A3B-4D73-B0CF-7EDA11A6208E}resource=/crio_Mod3/DO15;0;ReadMethodType=bool;WriteMethodType=bool{C84AA5A3-AE33-4F60-ADC0-D29FB88B09BC}resource=/crio_Mod2/DI6;0;ReadMethodType=bool{C9CA73E0-63C3-4C68-9989-E4607F431DFB}resource=/crio_Mod3/DO9;0;ReadMethodType=bool;WriteMethodType=bool{CA8A3767-5563-4B76-8BDD-BF64F2C5E18C}resource=/crio_Mod3/DO29;0;ReadMethodType=bool;WriteMethodType=bool{D1716271-BC52-4CFB-A1CB-4DD319909C94}resource=/crio_Mod1/DIO29;0;ReadMethodType=bool;WriteMethodType=bool{D1A5F91C-4BBF-4F76-A8FC-494DB7A80F1C}resource=/crio_Mod3/DO11;0;ReadMethodType=bool;WriteMethodType=bool{D24632BD-6A46-4F4D-945E-D3B03B760CEC}resource=/crio_Mod1/DIO30;0;ReadMethodType=bool;WriteMethodType=bool{D2739D94-6599-4C66-B0BC-7573C898C1D8}resource=/crio_Mod2/DI19;0;ReadMethodType=bool{D4C820BA-214C-4261-AF39-1E7A4EEBA11B}resource=/crio_Mod2/DI3;0;ReadMethodType=bool{D8251ED7-A8DE-4B1B-B736-9BBD8B70A376}resource=/crio_Mod2/DI7;0;ReadMethodType=bool{DA6B9BAD-0746-462F-8B71-E7DC5EF22156}resource=/crio_Mod2/DI17;0;ReadMethodType=bool{DC69BF39-7E28-482D-9916-8D1BA569C1A3}resource=/crio_Mod1/DIO17;0;ReadMethodType=bool;WriteMethodType=bool{E365D4CC-733A-4E1B-B300-0C3A3EAEF7BF}resource=/crio_Mod2/DI7:0;0;ReadMethodType=u8{E4DAA5DA-423C-4FF9-81AB-8C5D7EC73E1F}resource=/crio_Mod1/DIO2;0;ReadMethodType=bool;WriteMethodType=bool{E835C5A9-29BC-4DC4-B09C-4072DC2F670B}resource=/crio_Mod2/DI31:24;0;ReadMethodType=u8{EC08A3DE-A5D2-42F3-B78C-8150B624146D}resource=/crio_Mod1/DIO31:24;0;ReadMethodType=u8;WriteMethodType=u8{ECB54ED9-35A2-4048-B8A6-FF106DC23343}resource=/crio_Mod1/DIO3;0;ReadMethodType=bool;WriteMethodType=bool{ECC2B02E-CEF2-44C1-972E-63A21806D7CF}resource=/crio_Mod3/DO24;0;ReadMethodType=bool;WriteMethodType=bool{ECE0DC1D-63DC-4D37-BDC3-73C9897BB817}resource=/crio_Mod3/DO5;0;ReadMethodType=bool;WriteMethodType=bool{EE4ECB84-D604-4CF9-9717-A44A2DD5078A}resource=/crio_Mod1/DIO18;0;ReadMethodType=bool;WriteMethodType=bool{EF637D90-A90A-4205-9C34-3A104024B0C9}resource=/crio_Mod3/DO3;0;ReadMethodType=bool;WriteMethodType=bool{F2FD28E8-122B-4FA6-A8AA-17FE9E479C92}resource=/crio_Mod1/DIO6;0;ReadMethodType=bool;WriteMethodType=bool{F344E376-1815-48AF-8AAA-8ED87EDA87EB}resource=/crio_Mod2/DI1;0;ReadMethodType=bool{F46AFDBD-3B43-4103-A682-61502D49D837}resource=/crio_Mod1/DIO26;0;ReadMethodType=bool;WriteMethodType=bool{FC3571D6-DFB9-410B-A19A-900BAE0B19D9}resource=/crio_Mod1/DIO5;0;ReadMethodType=bool;WriteMethodType=bool{FD552EE8-48CA-480F-8812-C02835F0E0FB}resource=/crio_Mod3/DO0;0;ReadMethodType=bool;WriteMethodType=bool{FDE86930-B871-4FD7-B3A5-2A037BE5EC41}resource=/crio_Mod3/DO31:24;0;ReadMethodType=u8;WriteMethodType=u8{FE16717E-17E3-4EAF-83F2-6B6CAD0A0455}resource=/crio_Mod1/DIO16;0;ReadMethodType=bool;WriteMethodType=bool{FF495649-B381-408E-88CA-70310F362A6F}resource=/Sleep;0;ReadMethodType=bool;WriteMethodType=boolcRIO-9063/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9063FPGA_TARGET_FAMILYZYNQTARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
					<Property Name="configString.name" Type="Str">40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;Chassis Temperatureresource=/Chassis Temperature;0;ReadMethodType=i16cRIO-9063/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9063FPGA_TARGET_FAMILYZYNQTARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]I2C_SCLresource=/crio_Mod1/DIO31;0;ReadMethodType=bool;WriteMethodType=boolI2C_SDAresource=/crio_Mod1/DIO30;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO0resource=/crio_Mod1/DIO0;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO10resource=/crio_Mod1/DIO10;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO11resource=/crio_Mod1/DIO11;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO12resource=/crio_Mod1/DIO12;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO13resource=/crio_Mod1/DIO13;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO14resource=/crio_Mod1/DIO14;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO15:8resource=/crio_Mod1/DIO15:8;0;ReadMethodType=u8;WriteMethodType=u8Mod1/DIO15resource=/crio_Mod1/DIO15;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO16resource=/crio_Mod1/DIO16;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO17resource=/crio_Mod1/DIO17;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO18resource=/crio_Mod1/DIO18;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO19resource=/crio_Mod1/DIO19;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO1resource=/crio_Mod1/DIO1;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO20resource=/crio_Mod1/DIO20;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO21resource=/crio_Mod1/DIO21;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO22resource=/crio_Mod1/DIO22;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO23:16resource=/crio_Mod1/DIO23:16;0;ReadMethodType=u8;WriteMethodType=u8Mod1/DIO23resource=/crio_Mod1/DIO23;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO24resource=/crio_Mod1/DIO24;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO25resource=/crio_Mod1/DIO25;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO26resource=/crio_Mod1/DIO26;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO2resource=/crio_Mod1/DIO2;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO31:0resource=/crio_Mod1/DIO31:0;0;ReadMethodType=u32;WriteMethodType=u32Mod1/DIO31:24resource=/crio_Mod1/DIO31:24;0;ReadMethodType=u8;WriteMethodType=u8Mod1/DIO3resource=/crio_Mod1/DIO3;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO4resource=/crio_Mod1/DIO4;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO5resource=/crio_Mod1/DIO5;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO6resource=/crio_Mod1/DIO6;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO7:0resource=/crio_Mod1/DIO7:0;0;ReadMethodType=u8;WriteMethodType=u8Mod1/DIO7resource=/crio_Mod1/DIO7;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO8resource=/crio_Mod1/DIO8;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO9resource=/crio_Mod1/DIO9;0;ReadMethodType=bool;WriteMethodType=boolMod1[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 1,crio.Type=NI 9403,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.Initial Line Direction=00000000000000000000000000000000,cRIOModule.RsiAttributes=[crioConfig.End]Mod2/DI0resource=/crio_Mod2/DI0;0;ReadMethodType=boolMod2/DI10resource=/crio_Mod2/DI10;0;ReadMethodType=boolMod2/DI11resource=/crio_Mod2/DI11;0;ReadMethodType=boolMod2/DI12resource=/crio_Mod2/DI12;0;ReadMethodType=boolMod2/DI13resource=/crio_Mod2/DI13;0;ReadMethodType=boolMod2/DI14resource=/crio_Mod2/DI14;0;ReadMethodType=boolMod2/DI15:8resource=/crio_Mod2/DI15:8;0;ReadMethodType=u8Mod2/DI15resource=/crio_Mod2/DI15;0;ReadMethodType=boolMod2/DI16resource=/crio_Mod2/DI16;0;ReadMethodType=boolMod2/DI17resource=/crio_Mod2/DI17;0;ReadMethodType=boolMod2/DI18resource=/crio_Mod2/DI18;0;ReadMethodType=boolMod2/DI19resource=/crio_Mod2/DI19;0;ReadMethodType=boolMod2/DI1resource=/crio_Mod2/DI1;0;ReadMethodType=boolMod2/DI20resource=/crio_Mod2/DI20;0;ReadMethodType=boolMod2/DI21resource=/crio_Mod2/DI21;0;ReadMethodType=boolMod2/DI22resource=/crio_Mod2/DI22;0;ReadMethodType=boolMod2/DI23:16resource=/crio_Mod2/DI23:16;0;ReadMethodType=u8Mod2/DI23resource=/crio_Mod2/DI23;0;ReadMethodType=boolMod2/DI24resource=/crio_Mod2/DI24;0;ReadMethodType=boolMod2/DI25resource=/crio_Mod2/DI25;0;ReadMethodType=boolMod2/DI26resource=/crio_Mod2/DI26;0;ReadMethodType=boolMod2/DI27resource=/crio_Mod2/DI27;0;ReadMethodType=boolMod2/DI28resource=/crio_Mod2/DI28;0;ReadMethodType=boolMod2/DI29resource=/crio_Mod2/DI29;0;ReadMethodType=boolMod2/DI2resource=/crio_Mod2/DI2;0;ReadMethodType=boolMod2/DI30resource=/crio_Mod2/DI30;0;ReadMethodType=boolMod2/DI31:0resource=/crio_Mod2/DI31:0;0;ReadMethodType=u32Mod2/DI31:24resource=/crio_Mod2/DI31:24;0;ReadMethodType=u8Mod2/DI31resource=/crio_Mod2/DI31;0;ReadMethodType=boolMod2/DI3resource=/crio_Mod2/DI3;0;ReadMethodType=boolMod2/DI4resource=/crio_Mod2/DI4;0;ReadMethodType=boolMod2/DI5resource=/crio_Mod2/DI5;0;ReadMethodType=boolMod2/DI6resource=/crio_Mod2/DI6;0;ReadMethodType=boolMod2/DI7:0resource=/crio_Mod2/DI7:0;0;ReadMethodType=u8Mod2/DI7resource=/crio_Mod2/DI7;0;ReadMethodType=boolMod2/DI8resource=/crio_Mod2/DI8;0;ReadMethodType=boolMod2/DI9resource=/crio_Mod2/DI9;0;ReadMethodType=boolMod2[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 2,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Mod3/DO0resource=/crio_Mod3/DO0;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO10resource=/crio_Mod3/DO10;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO11resource=/crio_Mod3/DO11;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO12resource=/crio_Mod3/DO12;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO13resource=/crio_Mod3/DO13;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO14resource=/crio_Mod3/DO14;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO15:8resource=/crio_Mod3/DO15:8;0;ReadMethodType=u8;WriteMethodType=u8Mod3/DO15resource=/crio_Mod3/DO15;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO16resource=/crio_Mod3/DO16;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO17resource=/crio_Mod3/DO17;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO18resource=/crio_Mod3/DO18;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO19resource=/crio_Mod3/DO19;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO1resource=/crio_Mod3/DO1;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO20resource=/crio_Mod3/DO20;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO21resource=/crio_Mod3/DO21;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO22resource=/crio_Mod3/DO22;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO23:16resource=/crio_Mod3/DO23:16;0;ReadMethodType=u8;WriteMethodType=u8Mod3/DO23resource=/crio_Mod3/DO23;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO24resource=/crio_Mod3/DO24;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO25resource=/crio_Mod3/DO25;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO26resource=/crio_Mod3/DO26;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO27resource=/crio_Mod3/DO27;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO28resource=/crio_Mod3/DO28;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO29resource=/crio_Mod3/DO29;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO2resource=/crio_Mod3/DO2;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO30resource=/crio_Mod3/DO30;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO31:0resource=/crio_Mod3/DO31:0;0;ReadMethodType=u32;WriteMethodType=u32Mod3/DO31:24resource=/crio_Mod3/DO31:24;0;ReadMethodType=u8;WriteMethodType=u8Mod3/DO31resource=/crio_Mod3/DO31;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO3resource=/crio_Mod3/DO3;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO4resource=/crio_Mod3/DO4;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO5resource=/crio_Mod3/DO5;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO6resource=/crio_Mod3/DO6;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO7:0resource=/crio_Mod3/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8Mod3/DO7resource=/crio_Mod3/DO7;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO8resource=/crio_Mod3/DO8;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO9resource=/crio_Mod3/DO9;0;ReadMethodType=bool;WriteMethodType=boolMod3[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 3,crio.Type=NI 9476,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Scan Clockresource=/Scan Clock;0;ReadMethodType=boolSleepresource=/Sleep;0;ReadMethodType=bool;WriteMethodType=boolSPI_CLKresource=/crio_Mod1/DIO27;0;ReadMethodType=bool;WriteMethodType=boolSPI_MISOresource=/crio_Mod1/DIO29;0;ReadMethodType=bool;WriteMethodType=boolSPI_MOSIresource=/crio_Mod1/DIO28;0;ReadMethodType=bool;WriteMethodType=boolSystem Resetresource=/System Reset;0;ReadMethodType=bool;WriteMethodType=boolUSER FPGA LEDArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/USER FPGA LED;0;ReadMethodType=bool;WriteMethodType=bool</Property>
					<Property Name="NI.LV.FPGA.InterfaceBitfile" Type="Str">D:\SEN_DATAspace\KFT Laird\cRio-9063\FPGA Bitfiles\cRio-9063_FPGATarget_FPGAMainIBN_M6b97ZQZ-xQ.lvbitx</Property>
				</Item>
				<Item Name="FPGA_Main_TestI2C.vi" Type="VI" URL="../FPGA/FPGA_Main_TestI2C.vi">
					<Property Name="configString.guid" Type="Str">{00562116-B562-409C-9D30-D9F98544B9B3}resource=/crio_Mod1/DIO23:16;0;ReadMethodType=u8;WriteMethodType=u8{016B0988-FACD-4648-8FBD-B5999C42C5A9}resource=/crio_Mod2/DI0;0;ReadMethodType=bool{020C6902-295A-42E5-A17E-42A48A30F6DF}resource=/crio_Mod3/DO19;0;ReadMethodType=bool;WriteMethodType=bool{049C7703-D884-47E6-8225-D9BA5491C7BE}resource=/crio_Mod2/DI21;0;ReadMethodType=bool{05BC344C-F549-48A8-A11B-6072DE704315}resource=/crio_Mod1/DIO11;0;ReadMethodType=bool;WriteMethodType=bool{0D68321D-A04D-4330-A91B-D62B6D3AAD37}resource=/crio_Mod3/DO10;0;ReadMethodType=bool;WriteMethodType=bool{0D84AD1B-08FB-4C59-9274-F29D882CEBD1}resource=/crio_Mod3/DO12;0;ReadMethodType=bool;WriteMethodType=bool{0FABB4F2-3ED9-4F31-88BE-7683DC0511F5}resource=/crio_Mod2/DI2;0;ReadMethodType=bool{1026FEBA-ABDE-4063-80AC-E7979C6AD479}ResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;{10384E08-9379-4DE3-9AFD-FD59E80F5382}resource=/crio_Mod1/DIO31:0;0;ReadMethodType=u32;WriteMethodType=u32{1043F786-C175-4469-B1DE-BBDFE3875AF6}resource=/crio_Mod1/DIO0;0;ReadMethodType=bool;WriteMethodType=bool{17FE2A4B-3E1E-4107-989D-A6B93452E436}resource=/crio_Mod1/DIO22;0;ReadMethodType=bool;WriteMethodType=bool{1A1B4A41-1C62-4082-9D7E-2694353426B3}resource=/crio_Mod2/DI8;0;ReadMethodType=bool{1E8B4F9A-DCB8-4346-BBFF-B5610BF93087}resource=/crio_Mod3/DO22;0;ReadMethodType=bool;WriteMethodType=bool{22CF9362-D74A-428D-9C06-420365B75E48}resource=/crio_Mod2/DI15;0;ReadMethodType=bool{2736EF9F-3FDE-4EC3-B925-B76FE057F29A}resource=/crio_Mod2/DI28;0;ReadMethodType=bool{28CF67CC-658F-434A-BAC2-49DFB1255024}resource=/crio_Mod2/DI15:8;0;ReadMethodType=u8{2F11BDC7-11E0-41C8-A683-B0C490EC9644}resource=/crio_Mod1/DIO15:8;0;ReadMethodType=u8;WriteMethodType=u8{305B9F9D-1BD8-4EFE-A93E-39627D577B09}resource=/crio_Mod3/DO2;0;ReadMethodType=bool;WriteMethodType=bool{3499840D-45F4-4AEF-B32D-4C5DEAEB5594}resource=/System Reset;0;ReadMethodType=bool;WriteMethodType=bool{3573C28A-76EC-43BD-9BB4-F6C69A4C2E0E}resource=/crio_Mod1/DIO4;0;ReadMethodType=bool;WriteMethodType=bool{3775C84B-1696-4C28-9CB1-C6C153CB5FAF}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 1,crio.Type=NI 9403,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.Initial Line Direction=00000000000000000000000000000000,cRIOModule.RsiAttributes=[crioConfig.End]{3920DB94-9852-4D99-B5A2-B91AF7E1F3DA}resource=/crio_Mod2/DI29;0;ReadMethodType=bool{39D01142-1167-4B40-9F6C-326508ACB1D4}ArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/USER FPGA LED;0;ReadMethodType=bool;WriteMethodType=bool{3D5E2C27-0769-405C-AE48-1728CA31D088}resource=/crio_Mod1/DIO14;0;ReadMethodType=bool;WriteMethodType=bool{3DEC4547-8288-4A8B-85BF-E557F7090CA5}resource=/crio_Mod1/DIO27;0;ReadMethodType=bool;WriteMethodType=bool{3E31F480-7A99-4C40-8547-9FDE5EC4BFA0}resource=/crio_Mod3/DO15:8;0;ReadMethodType=u8;WriteMethodType=u8{41DD390A-A49A-4836-BAB7-D08F2E931A40}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 3,crio.Type=NI 9476,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{444F81C0-89E3-4436-AF38-969D0DDB7EA5}resource=/crio_Mod3/DO13;0;ReadMethodType=bool;WriteMethodType=bool{44D48A2F-DE4E-43B7-A8A0-48AB53C4BE07}resource=/crio_Mod2/DI4;0;ReadMethodType=bool{4507686C-A9EF-4760-872E-38EE1418284C}resource=/crio_Mod3/DO30;0;ReadMethodType=bool;WriteMethodType=bool{45CCAF41-8A26-4109-B0A8-DB5B3E65F598}resource=/crio_Mod3/DO23;0;ReadMethodType=bool;WriteMethodType=bool{4A50D079-2B6B-40CA-981E-5BB68E4E6072}resource=/crio_Mod3/DO8;0;ReadMethodType=bool;WriteMethodType=bool{4BA14884-0303-485D-81D5-57929C7D04A2}resource=/crio_Mod3/DO1;0;ReadMethodType=bool;WriteMethodType=bool{4F020830-0880-45E5-BBA5-C466C65B0BB4}resource=/crio_Mod2/DI22;0;ReadMethodType=bool{503BB1D4-A462-4F7E-8F3A-19AE7ACC8BF7}resource=/crio_Mod1/DIO13;0;ReadMethodType=bool;WriteMethodType=bool{53A9317E-5DC0-42DB-B02F-94872CCF750A}resource=/crio_Mod3/DO18;0;ReadMethodType=bool;WriteMethodType=bool{55370055-7E62-4CA9-8B8A-BA65B3CD81BE}resource=/Scan Clock;0;ReadMethodType=bool{56908C21-85E1-483A-A4A2-A4CD4D211160}resource=/crio_Mod2/DI23;0;ReadMethodType=bool{57004E1C-0A7E-436D-94AF-43971614D169}resource=/crio_Mod1/DIO31;0;ReadMethodType=bool;WriteMethodType=bool{58BC57D1-DA0A-41D8-9CB1-D5762A6F7CBF}resource=/crio_Mod1/DIO1;0;ReadMethodType=bool;WriteMethodType=bool{5CA7854A-B65B-4284-A594-88CA65F1DA70}resource=/crio_Mod1/DIO9;0;ReadMethodType=bool;WriteMethodType=bool{61E4317D-C641-47DF-8A0D-2DF226D62E0C}resource=/crio_Mod2/DI18;0;ReadMethodType=bool{639EC838-091A-4EAC-B912-ED76C3092A75}resource=/crio_Mod1/DIO7;0;ReadMethodType=bool;WriteMethodType=bool{64998FF0-55C1-425A-BC47-95B2A12DFE53}resource=/crio_Mod3/DO7;0;ReadMethodType=bool;WriteMethodType=bool{65FDCC92-2314-4885-8597-0389247683AC}resource=/crio_Mod2/DI14;0;ReadMethodType=bool{66A0CC07-B6D8-4DED-BB95-593C6B7F2417}resource=/crio_Mod2/DI23:16;0;ReadMethodType=u8{66C1D16C-B2CA-493D-BAA1-933236016BAC}resource=/crio_Mod2/DI30;0;ReadMethodType=bool{6B7D89C3-8C7B-48F0-AC09-69C08C302AF2}resource=/crio_Mod3/DO28;0;ReadMethodType=bool;WriteMethodType=bool{6C273585-5129-469A-8E97-BE843C7107E8}resource=/crio_Mod2/DI12;0;ReadMethodType=bool{6E41C8EE-A0D6-416C-985E-DD573E987F95}resource=/crio_Mod1/DIO10;0;ReadMethodType=bool;WriteMethodType=bool{6E874678-081B-4E03-8481-99EFDE2370A6}resource=/crio_Mod3/DO27;0;ReadMethodType=bool;WriteMethodType=bool{6E8B009E-A62B-445F-ADC9-E03128F40A3D}resource=/crio_Mod3/DO26;0;ReadMethodType=bool;WriteMethodType=bool{709E5FB9-7031-4AB5-9026-7B2232D2607E}resource=/crio_Mod1/DIO8;0;ReadMethodType=bool;WriteMethodType=bool{71A38E04-884D-4D5D-9104-308ECFA6F74D}resource=/crio_Mod3/DO31;0;ReadMethodType=bool;WriteMethodType=bool{71A8B016-7C1A-4EA1-B134-3CF7565BB050}resource=/crio_Mod1/DIO23;0;ReadMethodType=bool;WriteMethodType=bool{73E7C557-82F3-44F7-A10F-ABBD306026CC}resource=/crio_Mod1/DIO24;0;ReadMethodType=bool;WriteMethodType=bool{75652323-0379-4CA7-AC0A-4E5D69C21765}resource=/crio_Mod1/DIO12;0;ReadMethodType=bool;WriteMethodType=bool{79EF37B7-14ED-4F3C-8A4A-0E2D23122044}resource=/crio_Mod2/DI25;0;ReadMethodType=bool{7AB082CF-AE39-4FDB-840E-1173396BEB9A}resource=/crio_Mod3/DO21;0;ReadMethodType=bool;WriteMethodType=bool{7FE4A0B6-99DC-4785-A4DC-71239823C6CD}[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 2,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]{82D6749E-9F51-4098-B498-C578CDB8168F}resource=/crio_Mod3/DO20;0;ReadMethodType=bool;WriteMethodType=bool{83CB0575-DFF0-431D-8F4D-3484E4BA3F16}resource=/crio_Mod3/DO14;0;ReadMethodType=bool;WriteMethodType=bool{86183B77-902B-45CA-91EB-431F5FC9BE6B}resource=/crio_Mod1/DIO7:0;0;ReadMethodType=u8;WriteMethodType=u8{8AB5DED9-5500-4418-8C3A-04EF20B99D45}resource=/crio_Mod3/DO6;0;ReadMethodType=bool;WriteMethodType=bool{8B14372D-20D8-4991-9449-7AFECB3AFFF8}resource=/crio_Mod3/DO4;0;ReadMethodType=bool;WriteMethodType=bool{8D1935D2-A13D-4528-A1A0-BE34FA3AB959}resource=/crio_Mod3/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8{8DC3E634-168C-4EBD-A7A5-05FDDAAD782B}resource=/crio_Mod2/DI5;0;ReadMethodType=bool{8F871B33-CF83-434D-8102-7E3ADBD85E92}resource=/crio_Mod3/DO17;0;ReadMethodType=bool;WriteMethodType=bool{8FAD4D58-5FF4-42DA-B5A6-36DB1E792D0C}resource=/crio_Mod2/DI31:0;0;ReadMethodType=u32{95FEAD71-2493-47B2-BAAC-098CDA44775C}resource=/crio_Mod2/DI16;0;ReadMethodType=bool{9C0ABBBB-5A9C-4199-9197-352FF267DCFA}resource=/crio_Mod3/DO31:0;0;ReadMethodType=u32;WriteMethodType=u32{9E157F47-EAD0-405C-9061-20D3B0B1DEB2}resource=/crio_Mod2/DI20;0;ReadMethodType=bool{9E783D22-2967-4845-B592-BE55FBBBD0AC}resource=/crio_Mod3/DO16;0;ReadMethodType=bool;WriteMethodType=bool{A2425259-5555-4C85-96D3-1B9E1423B694}resource=/crio_Mod1/DIO19;0;ReadMethodType=bool;WriteMethodType=bool{A43CC4DA-D84E-49A1-9FC6-7FADE7FE0C58}resource=/crio_Mod2/DI9;0;ReadMethodType=bool{A62718E0-9738-4D92-8B82-89DA44B88A23}resource=/crio_Mod3/DO25;0;ReadMethodType=bool;WriteMethodType=bool{A656FFF7-2D24-4266-802E-6D256EED7BEE}resource=/crio_Mod2/DI13;0;ReadMethodType=bool{AEEAC7DE-70B0-4006-BBD3-90B734078461}resource=/crio_Mod2/DI11;0;ReadMethodType=bool{B35223B5-0430-4510-A949-BE672B40E6E6}resource=/crio_Mod2/DI27;0;ReadMethodType=bool{B5688A45-C76F-43E5-A99D-19580A6A66A0}resource=/crio_Mod2/DI26;0;ReadMethodType=bool{B7DA2C10-4585-44F3-8D2A-33EBBA04CFC3}resource=/crio_Mod1/DIO21;0;ReadMethodType=bool;WriteMethodType=bool{B86F1B95-4CEA-458A-9584-3DCA4908E079}resource=/crio_Mod2/DI31;0;ReadMethodType=bool{B9CF62AF-44D5-4C24-B58D-9607ECE30699}resource=/crio_Mod1/DIO15;0;ReadMethodType=bool;WriteMethodType=bool{BA05BD40-F9F9-4E0B-98B1-B48D09D8A676}resource=/crio_Mod2/DI24;0;ReadMethodType=bool{BB631661-AF2D-4F67-89E1-4590B39CAA78}resource=/crio_Mod1/DIO20;0;ReadMethodType=bool;WriteMethodType=bool{BBDA328F-FC1D-41A9-9686-96DF871D4641}resource=/Chassis Temperature;0;ReadMethodType=i16{BDCD4479-CB49-4497-B04E-3C430A0276A7}resource=/crio_Mod3/DO23:16;0;ReadMethodType=u8;WriteMethodType=u8{BE2238DD-36CE-41A7-B92C-26BED7784893}resource=/crio_Mod2/DI10;0;ReadMethodType=bool{BECD2180-70B7-4331-B9D7-7448A1065370}resource=/crio_Mod1/DIO28;0;ReadMethodType=bool;WriteMethodType=bool{C218AD58-F228-4954-AA2F-51C4FF56F2E2}resource=/crio_Mod1/DIO25;0;ReadMethodType=bool;WriteMethodType=bool{C7A29B4B-4A3B-4D73-B0CF-7EDA11A6208E}resource=/crio_Mod3/DO15;0;ReadMethodType=bool;WriteMethodType=bool{C84AA5A3-AE33-4F60-ADC0-D29FB88B09BC}resource=/crio_Mod2/DI6;0;ReadMethodType=bool{C9CA73E0-63C3-4C68-9989-E4607F431DFB}resource=/crio_Mod3/DO9;0;ReadMethodType=bool;WriteMethodType=bool{CA8A3767-5563-4B76-8BDD-BF64F2C5E18C}resource=/crio_Mod3/DO29;0;ReadMethodType=bool;WriteMethodType=bool{D1716271-BC52-4CFB-A1CB-4DD319909C94}resource=/crio_Mod1/DIO29;0;ReadMethodType=bool;WriteMethodType=bool{D1A5F91C-4BBF-4F76-A8FC-494DB7A80F1C}resource=/crio_Mod3/DO11;0;ReadMethodType=bool;WriteMethodType=bool{D24632BD-6A46-4F4D-945E-D3B03B760CEC}resource=/crio_Mod1/DIO30;0;ReadMethodType=bool;WriteMethodType=bool{D2739D94-6599-4C66-B0BC-7573C898C1D8}resource=/crio_Mod2/DI19;0;ReadMethodType=bool{D4C820BA-214C-4261-AF39-1E7A4EEBA11B}resource=/crio_Mod2/DI3;0;ReadMethodType=bool{D8251ED7-A8DE-4B1B-B736-9BBD8B70A376}resource=/crio_Mod2/DI7;0;ReadMethodType=bool{DA6B9BAD-0746-462F-8B71-E7DC5EF22156}resource=/crio_Mod2/DI17;0;ReadMethodType=bool{DC69BF39-7E28-482D-9916-8D1BA569C1A3}resource=/crio_Mod1/DIO17;0;ReadMethodType=bool;WriteMethodType=bool{E365D4CC-733A-4E1B-B300-0C3A3EAEF7BF}resource=/crio_Mod2/DI7:0;0;ReadMethodType=u8{E4DAA5DA-423C-4FF9-81AB-8C5D7EC73E1F}resource=/crio_Mod1/DIO2;0;ReadMethodType=bool;WriteMethodType=bool{E835C5A9-29BC-4DC4-B09C-4072DC2F670B}resource=/crio_Mod2/DI31:24;0;ReadMethodType=u8{EC08A3DE-A5D2-42F3-B78C-8150B624146D}resource=/crio_Mod1/DIO31:24;0;ReadMethodType=u8;WriteMethodType=u8{ECB54ED9-35A2-4048-B8A6-FF106DC23343}resource=/crio_Mod1/DIO3;0;ReadMethodType=bool;WriteMethodType=bool{ECC2B02E-CEF2-44C1-972E-63A21806D7CF}resource=/crio_Mod3/DO24;0;ReadMethodType=bool;WriteMethodType=bool{ECE0DC1D-63DC-4D37-BDC3-73C9897BB817}resource=/crio_Mod3/DO5;0;ReadMethodType=bool;WriteMethodType=bool{EE4ECB84-D604-4CF9-9717-A44A2DD5078A}resource=/crio_Mod1/DIO18;0;ReadMethodType=bool;WriteMethodType=bool{EF637D90-A90A-4205-9C34-3A104024B0C9}resource=/crio_Mod3/DO3;0;ReadMethodType=bool;WriteMethodType=bool{F2FD28E8-122B-4FA6-A8AA-17FE9E479C92}resource=/crio_Mod1/DIO6;0;ReadMethodType=bool;WriteMethodType=bool{F344E376-1815-48AF-8AAA-8ED87EDA87EB}resource=/crio_Mod2/DI1;0;ReadMethodType=bool{F46AFDBD-3B43-4103-A682-61502D49D837}resource=/crio_Mod1/DIO26;0;ReadMethodType=bool;WriteMethodType=bool{FC3571D6-DFB9-410B-A19A-900BAE0B19D9}resource=/crio_Mod1/DIO5;0;ReadMethodType=bool;WriteMethodType=bool{FD552EE8-48CA-480F-8812-C02835F0E0FB}resource=/crio_Mod3/DO0;0;ReadMethodType=bool;WriteMethodType=bool{FDE86930-B871-4FD7-B3A5-2A037BE5EC41}resource=/crio_Mod3/DO31:24;0;ReadMethodType=u8;WriteMethodType=u8{FE16717E-17E3-4EAF-83F2-6B6CAD0A0455}resource=/crio_Mod1/DIO16;0;ReadMethodType=bool;WriteMethodType=bool{FF495649-B381-408E-88CA-70310F362A6F}resource=/Sleep;0;ReadMethodType=bool;WriteMethodType=boolcRIO-9063/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9063FPGA_TARGET_FAMILYZYNQTARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]</Property>
					<Property Name="configString.name" Type="Str">40 MHz Onboard ClockResourceName=40 MHz Onboard Clock;TopSignalConnect=Clk40;ClockSignalName=Clk40;MinFreq=40000000,000000;MaxFreq=40000000,000000;VariableFreq=0;NomFreq=40000000,000000;PeakPeriodJitter=250,000000;MinDutyCycle=50,000000;MaxDutyCycle=50,000000;Accuracy=100,000000;RunTime=0;SpreadSpectrum=0;GenericDataHash=D41D8CD98F00B204E9800998ECF8427E;Chassis Temperatureresource=/Chassis Temperature;0;ReadMethodType=i16cRIO-9063/Clk40/falsefalseFPGA_EXECUTION_MODEFPGA_TARGETFPGA_TARGET_CLASSCRIO_9063FPGA_TARGET_FAMILYZYNQTARGET_TYPEFPGA/[rSeriesConfig.Begin][rSeriesConfig.End]I2C_SCLresource=/crio_Mod1/DIO31;0;ReadMethodType=bool;WriteMethodType=boolI2C_SDAresource=/crio_Mod1/DIO30;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO0resource=/crio_Mod1/DIO0;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO10resource=/crio_Mod1/DIO10;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO11resource=/crio_Mod1/DIO11;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO12resource=/crio_Mod1/DIO12;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO13resource=/crio_Mod1/DIO13;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO14resource=/crio_Mod1/DIO14;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO15:8resource=/crio_Mod1/DIO15:8;0;ReadMethodType=u8;WriteMethodType=u8Mod1/DIO15resource=/crio_Mod1/DIO15;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO16resource=/crio_Mod1/DIO16;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO17resource=/crio_Mod1/DIO17;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO18resource=/crio_Mod1/DIO18;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO19resource=/crio_Mod1/DIO19;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO1resource=/crio_Mod1/DIO1;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO20resource=/crio_Mod1/DIO20;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO21resource=/crio_Mod1/DIO21;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO22resource=/crio_Mod1/DIO22;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO23:16resource=/crio_Mod1/DIO23:16;0;ReadMethodType=u8;WriteMethodType=u8Mod1/DIO23resource=/crio_Mod1/DIO23;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO24resource=/crio_Mod1/DIO24;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO25resource=/crio_Mod1/DIO25;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO26resource=/crio_Mod1/DIO26;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO2resource=/crio_Mod1/DIO2;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO31:0resource=/crio_Mod1/DIO31:0;0;ReadMethodType=u32;WriteMethodType=u32Mod1/DIO31:24resource=/crio_Mod1/DIO31:24;0;ReadMethodType=u8;WriteMethodType=u8Mod1/DIO3resource=/crio_Mod1/DIO3;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO4resource=/crio_Mod1/DIO4;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO5resource=/crio_Mod1/DIO5;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO6resource=/crio_Mod1/DIO6;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO7:0resource=/crio_Mod1/DIO7:0;0;ReadMethodType=u8;WriteMethodType=u8Mod1/DIO7resource=/crio_Mod1/DIO7;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO8resource=/crio_Mod1/DIO8;0;ReadMethodType=bool;WriteMethodType=boolMod1/DIO9resource=/crio_Mod1/DIO9;0;ReadMethodType=bool;WriteMethodType=boolMod1[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 1,crio.Type=NI 9403,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.Initial Line Direction=00000000000000000000000000000000,cRIOModule.RsiAttributes=[crioConfig.End]Mod2/DI0resource=/crio_Mod2/DI0;0;ReadMethodType=boolMod2/DI10resource=/crio_Mod2/DI10;0;ReadMethodType=boolMod2/DI11resource=/crio_Mod2/DI11;0;ReadMethodType=boolMod2/DI12resource=/crio_Mod2/DI12;0;ReadMethodType=boolMod2/DI13resource=/crio_Mod2/DI13;0;ReadMethodType=boolMod2/DI14resource=/crio_Mod2/DI14;0;ReadMethodType=boolMod2/DI15:8resource=/crio_Mod2/DI15:8;0;ReadMethodType=u8Mod2/DI15resource=/crio_Mod2/DI15;0;ReadMethodType=boolMod2/DI16resource=/crio_Mod2/DI16;0;ReadMethodType=boolMod2/DI17resource=/crio_Mod2/DI17;0;ReadMethodType=boolMod2/DI18resource=/crio_Mod2/DI18;0;ReadMethodType=boolMod2/DI19resource=/crio_Mod2/DI19;0;ReadMethodType=boolMod2/DI1resource=/crio_Mod2/DI1;0;ReadMethodType=boolMod2/DI20resource=/crio_Mod2/DI20;0;ReadMethodType=boolMod2/DI21resource=/crio_Mod2/DI21;0;ReadMethodType=boolMod2/DI22resource=/crio_Mod2/DI22;0;ReadMethodType=boolMod2/DI23:16resource=/crio_Mod2/DI23:16;0;ReadMethodType=u8Mod2/DI23resource=/crio_Mod2/DI23;0;ReadMethodType=boolMod2/DI24resource=/crio_Mod2/DI24;0;ReadMethodType=boolMod2/DI25resource=/crio_Mod2/DI25;0;ReadMethodType=boolMod2/DI26resource=/crio_Mod2/DI26;0;ReadMethodType=boolMod2/DI27resource=/crio_Mod2/DI27;0;ReadMethodType=boolMod2/DI28resource=/crio_Mod2/DI28;0;ReadMethodType=boolMod2/DI29resource=/crio_Mod2/DI29;0;ReadMethodType=boolMod2/DI2resource=/crio_Mod2/DI2;0;ReadMethodType=boolMod2/DI30resource=/crio_Mod2/DI30;0;ReadMethodType=boolMod2/DI31:0resource=/crio_Mod2/DI31:0;0;ReadMethodType=u32Mod2/DI31:24resource=/crio_Mod2/DI31:24;0;ReadMethodType=u8Mod2/DI31resource=/crio_Mod2/DI31;0;ReadMethodType=boolMod2/DI3resource=/crio_Mod2/DI3;0;ReadMethodType=boolMod2/DI4resource=/crio_Mod2/DI4;0;ReadMethodType=boolMod2/DI5resource=/crio_Mod2/DI5;0;ReadMethodType=boolMod2/DI6resource=/crio_Mod2/DI6;0;ReadMethodType=boolMod2/DI7:0resource=/crio_Mod2/DI7:0;0;ReadMethodType=u8Mod2/DI7resource=/crio_Mod2/DI7;0;ReadMethodType=boolMod2/DI8resource=/crio_Mod2/DI8;0;ReadMethodType=boolMod2/DI9resource=/crio_Mod2/DI9;0;ReadMethodType=boolMod2[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 2,crio.Type=NI 9425,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Mod3/DO0resource=/crio_Mod3/DO0;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO10resource=/crio_Mod3/DO10;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO11resource=/crio_Mod3/DO11;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO12resource=/crio_Mod3/DO12;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO13resource=/crio_Mod3/DO13;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO14resource=/crio_Mod3/DO14;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO15:8resource=/crio_Mod3/DO15:8;0;ReadMethodType=u8;WriteMethodType=u8Mod3/DO15resource=/crio_Mod3/DO15;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO16resource=/crio_Mod3/DO16;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO17resource=/crio_Mod3/DO17;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO18resource=/crio_Mod3/DO18;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO19resource=/crio_Mod3/DO19;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO1resource=/crio_Mod3/DO1;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO20resource=/crio_Mod3/DO20;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO21resource=/crio_Mod3/DO21;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO22resource=/crio_Mod3/DO22;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO23:16resource=/crio_Mod3/DO23:16;0;ReadMethodType=u8;WriteMethodType=u8Mod3/DO23resource=/crio_Mod3/DO23;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO24resource=/crio_Mod3/DO24;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO25resource=/crio_Mod3/DO25;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO26resource=/crio_Mod3/DO26;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO27resource=/crio_Mod3/DO27;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO28resource=/crio_Mod3/DO28;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO29resource=/crio_Mod3/DO29;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO2resource=/crio_Mod3/DO2;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO30resource=/crio_Mod3/DO30;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO31:0resource=/crio_Mod3/DO31:0;0;ReadMethodType=u32;WriteMethodType=u32Mod3/DO31:24resource=/crio_Mod3/DO31:24;0;ReadMethodType=u8;WriteMethodType=u8Mod3/DO31resource=/crio_Mod3/DO31;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO3resource=/crio_Mod3/DO3;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO4resource=/crio_Mod3/DO4;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO5resource=/crio_Mod3/DO5;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO6resource=/crio_Mod3/DO6;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO7:0resource=/crio_Mod3/DO7:0;0;ReadMethodType=u8;WriteMethodType=u8Mod3/DO7resource=/crio_Mod3/DO7;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO8resource=/crio_Mod3/DO8;0;ReadMethodType=bool;WriteMethodType=boolMod3/DO9resource=/crio_Mod3/DO9;0;ReadMethodType=bool;WriteMethodType=boolMod3[crioConfig.Begin]crio.Calibration=1,crio.Location=Slot 3,crio.Type=NI 9476,cRIOModule.DisableArbitration=false,cRIOModule.EnableDECoM=false,cRIOModule.EnableInputFifo=false,cRIOModule.EnableOutputFifo=false,cRIOModule.RsiAttributes=[crioConfig.End]Scan Clockresource=/Scan Clock;0;ReadMethodType=boolSleepresource=/Sleep;0;ReadMethodType=bool;WriteMethodType=boolSPI_CLKresource=/crio_Mod1/DIO27;0;ReadMethodType=bool;WriteMethodType=boolSPI_MISOresource=/crio_Mod1/DIO29;0;ReadMethodType=bool;WriteMethodType=boolSPI_MOSIresource=/crio_Mod1/DIO28;0;ReadMethodType=bool;WriteMethodType=boolSystem Resetresource=/System Reset;0;ReadMethodType=bool;WriteMethodType=boolUSER FPGA LEDArbitrationForOutputData=NeverArbitrate;NumberOfSyncRegistersForOutputData=1;NumberOfSyncRegistersForReadInProject=Auto;resource=/USER FPGA LED;0;ReadMethodType=bool;WriteMethodType=bool</Property>
					<Property Name="NI.LV.FPGA.InterfaceBitfile" Type="Str">D:\SEN_DATAspace\KFT Laird\cRio-9063\FPGA Bitfiles\cRio-9063_FPGATarget_FPGAMainTestI2C_Gjc8ikaXbfw.lvbitx</Property>
				</Item>
				<Item Name="Dependencies" Type="Dependencies">
					<Item Name="vi.lib" Type="Folder">
						<Item Name="lvSimController.dll" Type="Document" URL="/&lt;vilib&gt;/rvi/Simulation/lvSimController.dll"/>
					</Item>
				</Item>
				<Item Name="Build Specifications" Type="Build">
					<Item Name="FPGA_Main" Type="{F4C5E96F-7410-48A5-BB87-3559BC9B167F}">
						<Property Name="AllowEnableRemoval" Type="Bool">false</Property>
						<Property Name="BuildSpecDecription" Type="Str"></Property>
						<Property Name="BuildSpecName" Type="Str">FPGA_Main</Property>
						<Property Name="Comp.BitfileName" Type="Str">cRio-9063_FPGATarget_FPGAMain_inOBxH1hPKc.lvbitx</Property>
						<Property Name="Comp.CustomXilinxParameters" Type="Str"></Property>
						<Property Name="Comp.MaxFanout" Type="Int">-1</Property>
						<Property Name="Comp.RandomSeed" Type="Bool">false</Property>
						<Property Name="Comp.Version.Build" Type="Int">0</Property>
						<Property Name="Comp.Version.Fix" Type="Int">0</Property>
						<Property Name="Comp.Version.Major" Type="Int">1</Property>
						<Property Name="Comp.Version.Minor" Type="Int">0</Property>
						<Property Name="Comp.VersionAutoIncrement" Type="Bool">false</Property>
						<Property Name="Comp.Vivado.EnableMultiThreading" Type="Bool">true</Property>
						<Property Name="Comp.Vivado.OptDirective" Type="Str"></Property>
						<Property Name="Comp.Vivado.PhysOptDirective" Type="Str"></Property>
						<Property Name="Comp.Vivado.PlaceDirective" Type="Str"></Property>
						<Property Name="Comp.Vivado.RouteDirective" Type="Str"></Property>
						<Property Name="Comp.Vivado.RunPowerOpt" Type="Bool">false</Property>
						<Property Name="Comp.Vivado.Strategy" Type="Str">Default</Property>
						<Property Name="Comp.Xilinx.DesignStrategy" Type="Str">balanced</Property>
						<Property Name="Comp.Xilinx.MapEffort" Type="Str">default(noTiming)</Property>
						<Property Name="Comp.Xilinx.ParEffort" Type="Str">standard</Property>
						<Property Name="Comp.Xilinx.SynthEffort" Type="Str">normal</Property>
						<Property Name="Comp.Xilinx.SynthGoal" Type="Str">speed</Property>
						<Property Name="Comp.Xilinx.UseRecommended" Type="Bool">true</Property>
						<Property Name="DefaultBuildSpec" Type="Bool">true</Property>
						<Property Name="DestinationDirectory" Type="Path">FPGA Bitfiles</Property>
						<Property Name="NI.LV.FPGA.LastCompiledBitfilePath" Type="Path">/D/SEN_DATAspace/KFT Laird/cRio-9063/FPGA Bitfiles/cRio-9063_FPGATarget_FPGAMain_inOBxH1hPKc.lvbitx</Property>
						<Property Name="NI.LV.FPGA.LastCompiledBitfilePathRelativeToProject" Type="Path">FPGA Bitfiles/cRio-9063_FPGATarget_FPGAMain_inOBxH1hPKc.lvbitx</Property>
						<Property Name="ProjectPath" Type="Path">/D/SEN_DATAspace/KFT Laird/cRio-9063/cRio-9063.lvproj</Property>
						<Property Name="RelativePath" Type="Bool">true</Property>
						<Property Name="RunWhenLoaded" Type="Bool">true</Property>
						<Property Name="SupportDownload" Type="Bool">true</Property>
						<Property Name="SupportResourceEstimation" Type="Bool">false</Property>
						<Property Name="TargetName" Type="Str">FPGA Target</Property>
						<Property Name="TopLevelVI" Type="Ref">/NI-cRIO-9063-01dc53a6/Chassis/FPGA Target/FPGA_Main.vi</Property>
					</Item>
					<Item Name="FPGA_Main_IBN" Type="{F4C5E96F-7410-48A5-BB87-3559BC9B167F}">
						<Property Name="AllowEnableRemoval" Type="Bool">false</Property>
						<Property Name="BuildSpecDecription" Type="Str"></Property>
						<Property Name="BuildSpecName" Type="Str">FPGA_Main_IBN</Property>
						<Property Name="Comp.BitfileName" Type="Str">cRio-9063_FPGATarget_FPGAMainIBN_M6b97ZQZ-xQ.lvbitx</Property>
						<Property Name="Comp.CustomXilinxParameters" Type="Str"></Property>
						<Property Name="Comp.MaxFanout" Type="Int">-1</Property>
						<Property Name="Comp.RandomSeed" Type="Bool">false</Property>
						<Property Name="Comp.Version.Build" Type="Int">0</Property>
						<Property Name="Comp.Version.Fix" Type="Int">0</Property>
						<Property Name="Comp.Version.Major" Type="Int">1</Property>
						<Property Name="Comp.Version.Minor" Type="Int">0</Property>
						<Property Name="Comp.VersionAutoIncrement" Type="Bool">false</Property>
						<Property Name="Comp.Vivado.EnableMultiThreading" Type="Bool">true</Property>
						<Property Name="Comp.Vivado.OptDirective" Type="Str"></Property>
						<Property Name="Comp.Vivado.PhysOptDirective" Type="Str"></Property>
						<Property Name="Comp.Vivado.PlaceDirective" Type="Str"></Property>
						<Property Name="Comp.Vivado.RouteDirective" Type="Str"></Property>
						<Property Name="Comp.Vivado.RunPowerOpt" Type="Bool">false</Property>
						<Property Name="Comp.Vivado.Strategy" Type="Str">Default</Property>
						<Property Name="Comp.Xilinx.DesignStrategy" Type="Str">balanced</Property>
						<Property Name="Comp.Xilinx.MapEffort" Type="Str">default(noTiming)</Property>
						<Property Name="Comp.Xilinx.ParEffort" Type="Str">standard</Property>
						<Property Name="Comp.Xilinx.SynthEffort" Type="Str">normal</Property>
						<Property Name="Comp.Xilinx.SynthGoal" Type="Str">speed</Property>
						<Property Name="Comp.Xilinx.UseRecommended" Type="Bool">true</Property>
						<Property Name="DefaultBuildSpec" Type="Bool">true</Property>
						<Property Name="DestinationDirectory" Type="Path">FPGA Bitfiles</Property>
						<Property Name="NI.LV.FPGA.LastCompiledBitfilePath" Type="Path">/D/SEN_DATAspace/KFT Laird/cRio-9063/FPGA Bitfiles/cRio-9063_FPGATarget_FPGAMainIBN_M6b97ZQZ-xQ.lvbitx</Property>
						<Property Name="NI.LV.FPGA.LastCompiledBitfilePathRelativeToProject" Type="Path">FPGA Bitfiles/cRio-9063_FPGATarget_FPGAMainIBN_M6b97ZQZ-xQ.lvbitx</Property>
						<Property Name="ProjectPath" Type="Path">/D/SEN_DATAspace/KFT Laird/cRio-9063/cRio-9063.lvproj</Property>
						<Property Name="RelativePath" Type="Bool">true</Property>
						<Property Name="RunWhenLoaded" Type="Bool">false</Property>
						<Property Name="SupportDownload" Type="Bool">true</Property>
						<Property Name="SupportResourceEstimation" Type="Bool">false</Property>
						<Property Name="TargetName" Type="Str">FPGA Target</Property>
						<Property Name="TopLevelVI" Type="Ref">/NI-cRIO-9063-01dc53a6/Chassis/FPGA Target/FPGA_Main_IBN.vi</Property>
					</Item>
					<Item Name="FPGA_Main_TestI2C" Type="{F4C5E96F-7410-48A5-BB87-3559BC9B167F}">
						<Property Name="AllowEnableRemoval" Type="Bool">false</Property>
						<Property Name="BuildSpecDecription" Type="Str"></Property>
						<Property Name="BuildSpecName" Type="Str">FPGA_Main_TestI2C</Property>
						<Property Name="Comp.BitfileName" Type="Str">cRio-9063_FPGATarget_FPGAMainTestI2C_Gjc8ikaXbfw.lvbitx</Property>
						<Property Name="Comp.CustomXilinxParameters" Type="Str"></Property>
						<Property Name="Comp.MaxFanout" Type="Int">-1</Property>
						<Property Name="Comp.RandomSeed" Type="Bool">false</Property>
						<Property Name="Comp.Version.Build" Type="Int">0</Property>
						<Property Name="Comp.Version.Fix" Type="Int">0</Property>
						<Property Name="Comp.Version.Major" Type="Int">1</Property>
						<Property Name="Comp.Version.Minor" Type="Int">0</Property>
						<Property Name="Comp.VersionAutoIncrement" Type="Bool">false</Property>
						<Property Name="Comp.Vivado.EnableMultiThreading" Type="Bool">true</Property>
						<Property Name="Comp.Vivado.OptDirective" Type="Str"></Property>
						<Property Name="Comp.Vivado.PhysOptDirective" Type="Str"></Property>
						<Property Name="Comp.Vivado.PlaceDirective" Type="Str"></Property>
						<Property Name="Comp.Vivado.RouteDirective" Type="Str"></Property>
						<Property Name="Comp.Vivado.RunPowerOpt" Type="Bool">false</Property>
						<Property Name="Comp.Vivado.Strategy" Type="Str">Default</Property>
						<Property Name="Comp.Xilinx.DesignStrategy" Type="Str">balanced</Property>
						<Property Name="Comp.Xilinx.MapEffort" Type="Str">default(noTiming)</Property>
						<Property Name="Comp.Xilinx.ParEffort" Type="Str">standard</Property>
						<Property Name="Comp.Xilinx.SynthEffort" Type="Str">normal</Property>
						<Property Name="Comp.Xilinx.SynthGoal" Type="Str">speed</Property>
						<Property Name="Comp.Xilinx.UseRecommended" Type="Bool">true</Property>
						<Property Name="DefaultBuildSpec" Type="Bool">true</Property>
						<Property Name="DestinationDirectory" Type="Path">FPGA Bitfiles</Property>
						<Property Name="NI.LV.FPGA.LastCompiledBitfilePath" Type="Path">/D/SEN_DATAspace/KFT Laird/cRio-9063/FPGA Bitfiles/cRio-9063_FPGATarget_FPGAMainTestI2C_Gjc8ikaXbfw.lvbitx</Property>
						<Property Name="NI.LV.FPGA.LastCompiledBitfilePathRelativeToProject" Type="Path">FPGA Bitfiles/cRio-9063_FPGATarget_FPGAMainTestI2C_Gjc8ikaXbfw.lvbitx</Property>
						<Property Name="ProjectPath" Type="Path">/D/SEN_DATAspace/KFT Laird/cRio-9063/cRio-9063.lvproj</Property>
						<Property Name="RelativePath" Type="Bool">true</Property>
						<Property Name="RunWhenLoaded" Type="Bool">false</Property>
						<Property Name="SupportDownload" Type="Bool">true</Property>
						<Property Name="SupportResourceEstimation" Type="Bool">false</Property>
						<Property Name="TargetName" Type="Str">FPGA Target</Property>
						<Property Name="TopLevelVI" Type="Ref">/NI-cRIO-9063-01dc53a6/Chassis/FPGA Target/FPGA_Main_TestI2C.vi</Property>
					</Item>
				</Item>
			</Item>
		</Item>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Generic FPGA Reference.ctl" Type="VI" URL="/&lt;vilib&gt;/myRIO/Common/Instrument Driver Framework/Utilities/typedefs/Generic FPGA Reference.ctl"/>
				<Item Name="Hardware Version Enum.ctl" Type="VI" URL="/&lt;vilib&gt;/myRIO/Common/Instrument Driver Framework/Utilities/typedefs/Hardware Version Enum.ctl"/>
				<Item Name="I2C Channels Enum.ctl" Type="VI" URL="/&lt;vilib&gt;/myRIO/Common/Instrument Driver Framework/myRIO v1.0/I2C/typedefs/I2C Channels Enum.ctl"/>
				<Item Name="I2C Channels FPGA Reference.ctl" Type="VI" URL="/&lt;vilib&gt;/myRIO/Common/Instrument Driver Framework/myRIO v1.0/I2C/typedefs/I2C Channels FPGA Reference.ctl"/>
				<Item Name="Internecine Avoider.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tcp.llb/Internecine Avoider.vi"/>
				<Item Name="lvSimController.dll" Type="Document" URL="/&lt;vilib&gt;/rvi/Simulation/lvSimController.dll"/>
				<Item Name="myRIO Generic Hardware Reference.ctl" Type="VI" URL="/&lt;vilib&gt;/myRIO/Common/Instrument Driver Framework/Utilities/typedefs/myRIO Generic Hardware Reference.ctl"/>
				<Item Name="myRIO v1.0 Configure I2C.vi" Type="VI" URL="/&lt;vilib&gt;/myRIO/Common/Instrument Driver Framework/myRIO v1.0/I2C/vis/myRIO v1.0 Configure I2C.vi"/>
				<Item Name="myRIO v1.0 Read I2C.vi" Type="VI" URL="/&lt;vilib&gt;/myRIO/Common/Instrument Driver Framework/myRIO v1.0/I2C/vis/myRIO v1.0 Read I2C.vi"/>
				<Item Name="myRIO v1.0 Reference.ctl" Type="VI" URL="/&lt;vilib&gt;/myRIO/Common/Instrument Driver Framework/Utilities/typedefs/myRIO v1.0 Reference.ctl"/>
				<Item Name="myRIO v1.0 Write I2C.vi" Type="VI" URL="/&lt;vilib&gt;/myRIO/Common/Instrument Driver Framework/myRIO v1.0/I2C/vis/myRIO v1.0 Write I2C.vi"/>
				<Item Name="nisyscfg.lvlib" Type="Library" URL="/&lt;vilib&gt;/nisyscfg/nisyscfg.lvlib"/>
				<Item Name="subTimeDelay.vi" Type="VI" URL="/&lt;vilib&gt;/express/express execution control/TimeDelayBlock.llb/subTimeDelay.vi"/>
				<Item Name="TCP Listen Internal List.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tcp.llb/TCP Listen Internal List.vi"/>
				<Item Name="TCP Listen List Operations.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/tcp.llb/TCP Listen List Operations.ctl"/>
				<Item Name="TCP Listen.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tcp.llb/TCP Listen.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
			</Item>
			<Item Name="cRio-9063_FPGATarget2_FPGAMain_X95IBVkBnlU.lvbitx" Type="Document" URL="../FPGA Bitfiles/cRio-9063_FPGATarget2_FPGAMain_X95IBVkBnlU.lvbitx"/>
			<Item Name="cRio-9063_FPGATarget_FPGAMain_inOBxH1hPKc.lvbitx" Type="Document" URL="../FPGA Bitfiles/cRio-9063_FPGATarget_FPGAMain_inOBxH1hPKc.lvbitx"/>
			<Item Name="Falling Edge Trigger.vi" Type="VI" URL="../Sub VIs/Utilities/Falling Edge Trigger.vi"/>
			<Item Name="NiFpgaLv.dll" Type="Document" URL="NiFpgaLv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="RT_Main" Type="{69A947D5-514E-4E75-818E-69657C0547D8}">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{C7F71F3E-BE46-47F3-9A96-81C026E01062}</Property>
				<Property Name="App_INI_GUID" Type="Str">{608CACF1-C8A4-479F-B840-C1739077CE6B}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{464EAA89-BD71-44B4-B696-9182869B04FD}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">RT_Main</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/NI_AB_TARGETNAME/RT_Main</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{DF8604FB-68B9-4E23-8AB3-91AA2707CB17}</Property>
				<Property Name="Bld_targetDestDir" Type="Path">/home/lvuser/natinst/bin</Property>
				<Property Name="Bld_version.build" Type="Int">6</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">startup.rtexe</Property>
				<Property Name="Destination[0].path" Type="Path">/home/lvuser/natinst/bin/startup.rtexe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">/home/lvuser/natinst/bin/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{2232DFA8-E5FA-4ED8-9BF6-C0AFF4A55C1B}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/NI-cRIO-9063-01dc53a6/RT/Run On Target multiple Connections.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_fileDescription" Type="Str">RT_Main</Property>
				<Property Name="TgtF_internalName" Type="Str">RT_Main</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2019 </Property>
				<Property Name="TgtF_productName" Type="Str">RT_Main</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{83B774CF-ADD6-4DB6-A469-6BDDC91BE03A}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">startup.rtexe</Property>
				<Property Name="TgtF_versionIndependent" Type="Bool">true</Property>
			</Item>
		</Item>
	</Item>
</Project>
